<?php

/*

ARQUIVO view.php

Responsável pela exibição para o usuário.

*/
include_once( 'model.php' );

include_once( './view/components/autocomplete.php' );
include_once( './view/components/grid.php' );
include_once( './view/components/menu.php' );
include_once( './view/components/selector.php' );
include_once( './view/components/footer.php' );


function merge_views ( $content, $private ) {

  // View com id 1 reservado para template default para todas as páginas
  $view_1 = read( 'views', array( '*' ), $conditions= new Condition( 'id', '==', 1 ) )[ 0 ];
  // print( '<!--' . print_r( $view_1, true ) . '-->' );


  // $private = false;
  if ( $private ) {

    $view_2 = read( 'views', array( '*' ), $conditions= new Condition( 'id', '==', 2 ) )[ 0 ];
    // print( "<!-- view_2: ". print_r( $view_2, true ) . "-->" );

    $merged = str_replace( '$content', $view_2[ 'html' ], $view_1[ 'html' ] );

  }
  else {

    $merged = $view_1[ 'html' ];

  }

  // print( "<!-- merged before:  $merged -->" );
  // print( "<!-- content:  $content -->" );

  $merged = str_replace( '$content', $content, $merged );

  // print( "<!-- merged after:  $merged -->" );

  return $merged;

}

function forge_entity_table ( $row, $private )  {

  $dom = new DOMDocument( '1.0' );

  $table = $dom->createElement( 'table' );
  $dom->appendChild( $table );

  $tbody = $dom->createElement( 'tbody' );
  $table->appendChild( $tbody );

  // print_r( $row );

  foreach ( $row as $column => $data )  {

    // print( $data );

    $tr = $dom->createElement( 'tr' );
    $tbody->appendChild( $tr );

    $td = $dom->createElement( 'td', $column );
    $tr->appendChild( $td );

    $td = $dom->createElement( 'td', print_r( $data, true ) );
    $tr->appendChild( $td );

  }

  $result = $dom->saveHTML();
  // $result = merge_views( $dom->saveHTML(), $private );

  return $result;

}

function clean_dom_saveHTML( $html ) {

  $html = str_replace( '%24api_key', '$api_key', $html );

  $html = html_entity_decode( $html );

  return preg_replace( '/^<!DOCTYPE.+?>/', '' , str_replace( array( '<html>', '</html>', '<body>', '</body>' ), array( '', '', '', '' ), $html ) );

}

function staticfy_html ( $html, $field_fillers= null )  {

  trigger_error( 'html: ' . $html );
  trigger_error( 'field_fillers: ' . print_r( $field_fillers, true ) );

  $dom = new DOMDocument( '1.0', 'utf-8' );
  @$dom->loadHTML( '<?xml encoding="UTF-8">' . $html, /*LIBXML_HTML_NOIMPLIED |*/ LIBXML_HTML_NODEFDTD );

  // trigger_error( "saveHTML: ". $dom->saveHTML() );

  $xpath = new DOMXPath( $dom );

  $staticfiers = array(
    'Grid'                => 'Grid',
    'AutoComplete'        => 'AutoComplete',
    'MediaFilesSelector'  => 'MediaFilesSelector',
    'Selector'            => 'Selector',
    'Menu'                => 'Menu',
    'Footer'              => 'Footer'
  );


  foreach( $staticfiers as $class => $staticfier )  {

    $nodes = $xpath->query( "//*[@class='$class']" );
    // trigger_error( $class . ' nodes: ' . print_r( $nodes, true ) );

    foreach( $nodes as $node )  {
      $parent = $node->parentNode;

      $staticfier_obj = new $staticfier( $node );
      // trigger_error( 'stactiobj: '. print_r( $staticfier_obj, true ) );

      $output = $staticfier_obj->output();
      // trigger_error( "output: " . $output );

      $output_dom = new DOMDocument( '1.0', 'utf-8' );
      @$output_dom->loadHTML( '<?xml encoding="UTF-8">' . $output, LIBXML_HTML_NODEFDTD );

      $root_element = $output_dom->documentElement;

      // print( "<!-- output_dom: " . $output_dom->saveHTML( $root_element ) . " -->" );

      $output_xpath = new DOMXPath( $output_dom );
      $body_children = $output_xpath->query( "//body/*" );

      foreach( $body_children as $child ) {

        $imported = $dom->importNode( $child, true );
        $parent->insertBefore( $imported, $node );

      }

      $parent->removeChild( $node );

    }

  }

  if ( $field_fillers != null )  {

    foreach ( $field_fillers as $column_name => $column_value )  {

      trigger_error( 'field_filler: ' . $column_name . ' - ' . print_r( $column_value, true ) );

      $field_nodes = $xpath->query( "//*[@name='$column_name']" );
      $field_node = $field_nodes[ 0 ];

      // trigger_error( "field_node: " . print_r( $field_node, true ) );

      $field_node->setAttribute( 'value', $column_value );

    }

  }

  $root_element = $dom->documentElement;
  $html_fragment = clean_dom_saveHTML( $dom->saveHTML( $root_element ) );

  return $html_fragment;

}


class MediaFilesSelector  {

  public function __construct ( $node ) {

    $this->node = $node;

    $template = '<div class="MediaFilesSelector">
  <table class="MediaFilesSelector_files_table">
    <thead>
      <tr>
        <th>Nome do Arquivo</th>
        <th>Excluir</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <br>
  <div>
    <input type="text" onfocusin="new MediaFilesSelector.AutoComplete( this );" readonly>
  </div>
</div>';

    $this->template = $template;

  }

  public function output  ()  {

    $output = $this->template;

    return $output;

  }

}


?>
