<?php

/*

ARQUIVO model.php

É responsável pela modelagem, ou seja, estruturação, validação e armazenamento
dos dados. Utiliza a classe PDO para interações com a base de dados.

*/

include_once( 'config.php' );


/*

Configura uma conexão com o banco de dados.
Retorna um objeto PDO.

*/
function setup_pdo ()  {

  global $PDO_config;

  // Extrai $PDO setando $host, $port, $database, $username, $password
  extract( $PDO_config );
  $dsn = "mysql:host={$host};port={$port};dbname={$database};charset=utf8";

  return ( new PDO( $dsn, $username, $password ) );

}


function fetch_all ( $query, $bindings= null ) {

  global $PDO;

  $stmt = $PDO->prepare( $query );

  if ( $bindings === null )  {
    $stmt->execute();
  }
  else {
    $stmt->execute( $bindings );
  }

  $result = $stmt->fetchAll( PDO::FETCH_ASSOC );

  return $result;

}

function execute ( $query, $bindings= null ) {

  global $PDO;

  $stmt = $PDO->prepare( $query );

  if ( $bindings === null )
    return $stmt->execute();

  else
    return $stmt->execute( $bindings );

}


function create( $table, $data )  {

  $bindings = array();

  $xkeys = array_keys( $data );
  $keys = array();
  foreach( $xkeys as $key => $value ) {

    $keys[ $key ] = "`$value`";

  }
  $columns_str = implode( ', ', $keys );

  // print( '$columns_str: '. print_r( $columns_str, true ). '<br>' );

  $pdo_substs = array();
  foreach( $data as $key => $value ) {

    array_push( $bindings, $value );
    array_push( $pdo_substs, '?' );

  }
  $substs_str = implode( ', ', $pdo_substs );

  $query = 'INSERT INTO ' . $table . ' (' . $columns_str . ') VALUES (' . $substs_str . ') ;';

  // print( 'INSERT $query: ' . $query . '<br>' )/;
  // print( 'INSERT $bindings: ' . print_r( $bindings, true ) . '<br>' );

  return execute( $query, $bindings );

}

function read ( $table, $columns, $conditions= null, $sorting= null, $splitting= null, $joins= null  ) {

  $bindings = array();

  // Junta valores da array columns separando por vírgulas numa string
  $columns_str = implode( ', ', $columns );

  $query = 'SELECT ' . $columns_str . ' FROM ' . $table;

  if ( $joins !== null )  {

      foreach ( $joins as $each_join )  {

          if ( $each_join instanceof InnerJoin )  {

              $query .= " INNER JOIN {$each_join->right_table} ON `{$each_join->left_table}`.`{$each_join->left_column}` = `{$each_join->right_table}`.`{$each_join->right_column}`";

          }
          elseif ( $each_join instanceof LeftJoin ) {

              $query .= " LEFT JOIN {$each_join->right_table} ON `{$each_join->left_table}`.`{$each_join->left_column}` = `{$each_join->right_table}`.`{$each_join->right_column}`";

          }
          elseif ( $each_join instanceof RightJoin ) {

              $query .= " RIGHT JOIN {$each_join->right_table} ON `{$each_join->left_table}`.`{$each_join->left_column}` = `{$each_join->right_table}`.`{$each_join->right_column}`";

          }
          elseif ( $each_join instanceof FullOuterJoin ) {

              $query .= " FULL OUTER JOIN {$each_join->right_table} ON `{$each_join->left_table}`.`{$each_join->left_column}` = `{$each_join->right_table}`.`{$each_join->right_column}`";

          }
          else {

              throw new Exception ( 'Unknown Join type' );

          }

      }

  }

  if ( $conditions !== null ) {

    // print_r( $conditions );

    $evaled = eval_Condition( $conditions );

    $output = $evaled[ 0 ];

    $having = false;
    foreach ( $columns as $column ) {

      if ( strpos( $column, ' AS ' ) !== false ) {
        $having = true;
        break;
      }

    }

    if ( $having )  {
      $query .= ' HAVING ' . $output;
    }
    else {
      $query .= ' WHERE ' . $output;
    }


    $bindings += $evaled[ 1 ];

  }

  if ( $sorting !== null )  {

    $output = '';
    foreach ( $sorting as $column => $direction )  {

      if ( $output != '' )  {
        $output .= ", ";
      }
      $output .= "`$column` $direction";

    }

    $query .= ' ORDER BY '. $output;

  }

  if ( $splitting !== null )  {

    $query .= " LIMIT {$splitting[ 0 ]}, {$splitting[ 1 ]}";

  }

  $query .= ' ;';

  // print( '<!-- SELECT $query: ' . $query . " -->\n" );
  // print( '<!-- SELECT $bindings: ' . print_r( $bindings, true ) . " -->\n" );

  $result = fetch_all( $query, $bindings );

  if ( ! is_array( $result ) )
    return false;

  // print( '<!-- select result: '. print_r( $result, true ) . " -->\n" );

  return $result ;

}

function update ( $table, $data, $conditions )  {

  $bindings = array();

  // print_r( $data );

  $sets = array();
  foreach ( $data as $key => $value )  {
    array_push( $sets, "`$key` = ?" );
    array_push( $bindings, $value );
  }
  $sets_str = implode( ', ', $sets );

  $evaled = eval_Condition( $conditions );
  $output = $evaled[ 0 ];
  $conditions_str = ' WHERE ' . $output;

  // print( 'bindings 1: ' . print_r( $bindings, true ) );
  // print( 'evaled 1: ' . print_r( $evaled[ 1 ], true ) );

  $bindings = array_merge( $bindings, $evaled[ 1 ] );

  /*
  print( 'bindings 2: ' . print_r( $bindings, true ) );

  $x = array( 'blablabla' );
  $y = array( 'yayaya' );
  $x += $y;
  print( '$x: ' . print_r( $x, true ) );
  */

  $query = 'UPDATE ' . $table . ' SET ' . $sets_str .$conditions_str;

  // print_r( 'UPDATE $query: ' . $query . '<br>' );
  // print_r( 'UPDATE $bindings: ' . print_r( $bindings, true ) . '<br>');

  return execute( $query, $bindings );

}

function delete ( $table, $conditions ) {

  $bindings = array();

  $evaled = eval_Condition( $conditions );
  $output = $evaled[ 0 ];
  $conditions_str = ' WHERE ' . $output;

  $bindings = array_merge( $bindings, $evaled[ 1 ] );

  $query = 'DELETE FROM ' . $table .$conditions_str;

  return execute( $query, $bindings );

}

function eval_Condition ( $Condition )  {

  $operator = $Condition->operator;
  if( $operator == '==' )  {
    $operator = '=';
  }

  // print( $operator );
  $bindings = array();
  if ( $Condition->one instanceof Condition )  {

    $evaled = eval_Condition( $Condition->one );
    $one_sql = $evaled[ 0 ];

    foreach( $evaled[ 1 ] as $new_binding ) {
      array_push( $bindings, $new_binding );
    }

  }
  else {
    $one_sql = "`{$Condition->one}`";
  }

  if ( $Condition->two instanceof Condition )  {

    $evaled = eval_Condition( $Condition->two );
    $two_sql = $evaled[ 0 ];

    foreach( $evaled[ 1 ] as $new_binding ) {
      array_push( $bindings, $new_binding );
    }

  }
  else {

    $two_sql = '?';
    $bindings[] = $Condition->two;

  }

  $output = $one_sql . ' ' . $operator . ' ' . $two_sql ;

  $arr = array( $output, $bindings );
  return $arr;

}


class Condition {

  public $one;
  public $operator;
  public $two;

  public function __construct ( $one, $operator, $two )  {

    // Testa se operator é válido
    $valid_operators = array( '==', 'LIKE', 'AND' );
    if ( ! in_array( $operator, $valid_operators ) )
      throw new Exception( "Invalid operator: {$operator}" );

    $this->one = $one;
    $this->operator = $operator;
    $this->two = $two;

  }

}


class _Join {

  public function __construct ( $left_table, $left_column, $right_table, $right_column )  {

      $this->left_table = $left_table;
      $this->left_column = $left_column;

      $this->right_table = $right_table;
      $this->right_column = $right_column;

  }

}

class InnerJoin extends _Join {}

class FullOuterJoin extends _Join {}

class LeftJoin extends _Join {}

class RightJoin extends _Join {}



function get_columns( $table_name )  {

  global $PDO_config;

  $table_schema = $PDO_config[ 'database' ];

  $query = "SELECT column_name FROM information_schema.columns where table_schema = '$table_schema' and table_name = '$table_name';";
  $fetch = fetch_all( $query );

  $columns = array();
  foreach( $fetch as $row ) {
    array_push( $columns, $row[ 'column_name' ] );
  }

  return $columns;

}


$PDO = setup_pdo();


?>
