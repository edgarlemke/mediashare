#!/bin/bash


echo "Script de Configuração para Servidor MediaShare"
echo "versão 1.0 - GPLv3 - por Edgar Roberto Lemke <edgar_lemke@yahoo.com.br>"
echo ""


# Verifica usuário que executa o script é root
if [ `id -u` -ne 0 ]; then
  echo "Execute este script como root."
  exit
fi


# Instala pacotes necessários
echo "Configurando pacotes necessários..."
echo ""
sleep 5

apt-get update

all_yes="-y --allow-downgrades --allow-remove-essential --allow-change-held-packages"

apt full-upgrade $all_yes

install="apt-get install $all_yes"

$install apache2 mariadb-server
$install php7.0 php7.0-mysql php7.0-xml


echo ""
sleep 5


# Copia MediaShare para diretório
echo "Copiando arquivos do MediaShare para diretório do servidor web Apache..."
echo ""
sleep 5

cp -rv ./MediaShare/ /var/www/html/
chown -R www-data:www-data /var/www/html/MediaShare/uploads/

echo ""


# Configura PHP
echo "Configurando PHP..."
echo ""
sleep 5

php_ini=$(find /etc/php | grep apache2 | grep php.ini)

sed -i "s/post_max_size = 8M/post_max_size = 257M/g" "$php_ini"
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 256M/g" "$php_ini"
sed -i "s/memory_limit = 256M/memory_limit = 512M/g" "$php_ini"


# Configura Apache
echo "Configurando servidor web Apache..."
echo ""
sleep 5


cp -v ./apache2-mediashare.conf /etc/apache2/sites-available/mediashare.conf

a2enmod rewrite
a2ensite mediashare

systemctl restart apache2

echo ""
echo ""


# Configura MariaDB

echo "Configurando banco de dados MariaDB..."
echo ""
sleep 5

# Pede senha root
echo -n "Digite a senha root do MariaDB: "
read -s mariadb_root_passwd
echo ""
echo ""

# Gera senha aleatória
mariadb_mediashare_passwd="$(openssl rand -base64 24 | tr -dc 'a-zA-Z0-9' | head -c 12 )"
echo "Senha do usuário mediashare: $mariadb_mediashare_passwd"
echo ""

# Envia comandos ao MariaDB
echo "Criando usuário mediashare no banco de dados..."
echo ""
sleep 5

mysql="mysql --user=root --password=\"$mariadb_root_passwd\" -e"

$mysql "CREATE USER \`mediashare\`@localhost IDENTIFIED BY '$mariadb_mediashare_passwd';"
$mysql "GRANT ALL PRIVILEGES ON \`mediashare\`.* TO \`mediashare\`@localhost;"
$mysql "FLUSH PRIVILEGES;"

# Importa base de dados padrão
echo "Importando base de dados padrão..."
echo ""
sleep 5
mysql --user=root --password="$mariadb_root_passwd" < default.sql


# Altera senha de conexão no arquivo config.php
echo "Atualizando config.php com nova senha do banco de dados..."
echo ""
sleep 5

sed -i "s/PASSWORD_GOES_HERE/$mariadb_mediashare_passwd/g" /var/www/html/MediaShare/config.php


echo "Okay, tudo pronto!"
