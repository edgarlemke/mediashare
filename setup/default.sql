/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE `mediashare`;

USE `mediashare`;


--
-- Table structure for table `api_credentials`
--

DROP TABLE IF EXISTS `api_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_credentials` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `key` char(64) NOT NULL,
  `user_id` int(4) DEFAULT NULL,
  `expiration_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `api_credentials_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_user_id_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_credential_id` int(11) NOT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_credential_id` (`api_credential_id`),
  CONSTRAINT `cache_ibfk_1` FOREIGN KEY (`api_credential_id`) REFERENCES `api_credentials` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `media_files`
--

DROP TABLE IF EXISTS `media_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `path` varchar(255) NOT NULL,
  `hash` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `media_files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `media_files_media_groups`
--

DROP TABLE IF EXISTS `media_files_media_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_files_media_groups` (
  `media_file_id` int(11) NOT NULL,
  `media_group_id` int(11) NOT NULL,
  KEY `fk_media_files` (`media_file_id`),
  KEY `fk_media_groups` (`media_group_id`),
  CONSTRAINT `fk_media_files` FOREIGN KEY (`media_file_id`) REFERENCES `media_files` (`id`),
  CONSTRAINT `fk_media_groups` FOREIGN KEY (`media_group_id`) REFERENCES `media_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `media_groups`
--

DROP TABLE IF EXISTS `media_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `media_groups` WRITE;
/*!40000 ALTER TABLE `media_groups` DISABLE KEYS */;
INSERT INTO `media_groups` VALUES (1,'Padrão');
/*!40000 ALTER TABLE `media_groups` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `media_groups_schedulings`
--

DROP TABLE IF EXISTS `media_groups_schedulings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_groups_schedulings` (
  `scheduling_id` int(11) NOT NULL,
  `media_group_id` int(11) NOT NULL,
  KEY `scheduling_id` (`scheduling_id`),
  KEY `media_group_id` (`media_group_id`),
  CONSTRAINT `media_groups_schedulings_ibfk_1` FOREIGN KEY (`scheduling_id`) REFERENCES `schedulings` (`id`),
  CONSTRAINT `media_groups_schedulings_ibfk_2` FOREIGN KEY (`media_group_id`) REFERENCES `media_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `media_groups_units`
--

DROP TABLE IF EXISTS `media_groups_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_groups_units` (
  `media_group_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  KEY `fk_media_groups_units_media_group_id` (`media_group_id`),
  KEY `fk_media_groups_units_unit_id` (`unit_id`),
  CONSTRAINT `fk_media_groups_units_media_group_id` FOREIGN KEY (`media_group_id`) REFERENCES `media_groups` (`id`),
  CONSTRAINT `fk_media_groups_units_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inclusion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hostname` varchar(64) NOT NULL,
  `hardware` varchar(64) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `client_version` varchar(16) NOT NULL,
  `client_key` char(64) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `vendor` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unit_id`),
  CONSTRAINT `players_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `players_config`
--

DROP TABLE IF EXISTS `players_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lease_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players_config`
--

LOCK TABLES `players_config` WRITE;
/*!40000 ALTER TABLE `players_config` DISABLE KEYS */;
INSERT INTO `players_config` VALUES (1,15);
/*!40000 ALTER TABLE `players_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players_reports`
--

DROP TABLE IF EXISTS `players_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(15) NOT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `player_id` (`player_id`),
  CONSTRAINT `players_reports_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `URI` varchar(64) NOT NULL,
  `entity_name` varchar(32) DEFAULT NULL,
  `entity_id` int(4) unsigned DEFAULT NULL,
  `view_id` int(4) unsigned DEFAULT NULL,
  `private` int(1) NOT NULL DEFAULT '1',
  `merge_view` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `view_id` (`view_id`),
  CONSTRAINT `resources_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`),
  CONSTRAINT `resources_ibfk_2` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` (`id`, `URI`, `entity_name`, `entity_id`, `view_id`, `private`, `merge_view`) VALUES
(1,	'/',	NULL,	NULL,	3,	0,	1),
(2,	'/home',	NULL,	NULL,	4,	1,	1),
(3,	'/players_config/1',	'players_config',	1,	5,	1,	1),
(4,	'/media_files',	'media_files',	NULL,	6,	1,	1),
(5,	'/media_groups',	'media_groups',	NULL,	7,	1,	1),
(6,	'/schedulings',	'schedulings',	NULL,	8,	1,	1),
(7,	'/users',	'users',	NULL,	9,	1,	1),
(8,	'/units',	'units',	NULL,	10,	1,	1),
(9,	'/units/register_screen',	'units',	NULL,	11,	1,	0),
(10,	'/users/register_screen',	'users',	NULL,	12,	1,	0),
(11,	'/schedulings/register_screen',	'schedulings',	NULL,	13,	1,	0),
(12,	'/media_groups/register_screen',	'media_groups',	NULL,	14,	1,	0),
(13,	'/players_config/1/edit',	'players_config',	1,	5,	1,	1),

(14, '/units/1', 'units', 1, NULL, 1, 1 ),
(15, '/units/1/edit', 'units', 1, 15, 1, 1 ),

(16, '/users/1', 'users', 1, NULL, 1, 1 ),
(17, '/users/1/edit', 'users', 1, 16, 1, 1 ),

(18, '/media_groups/1', 'media_groups', 1, NULL, 1, 1 ),
(19, '/media_groups/1/edit', 'media_groups', 1, 18, 1, 1 ),

(20, '/players', 'players', NULL, NULL, 1, 1),
(21, '/players_reports', 'players_reports', NULL, NULL, 1, 1);

/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources_valid_methods`
--

DROP TABLE IF EXISTS `resources_valid_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_valid_methods` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(8) NOT NULL,
  `resource_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `FK_resource_id_resources_id` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`),
  CONSTRAINT `resources_valid_methods_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_valid_methods`
--

LOCK TABLES `resources_valid_methods` WRITE;
/*!40000 ALTER TABLE `resources_valid_methods` DISABLE KEYS */;
INSERT INTO `resources_valid_methods` (`id`, `method`, `resource_id`) VALUES
(1,	'GET',	1),
(2,	'GET',	2),
(3,	'GET',	3),
(4,	'PATCH',	3),
(5,	'GET',	4),
(6,	'POST',	4),
(7,	'GET',	5),
(8,	'POST',	5),
(9,	'GET',	6),
(10,	'POST',	6),
(11,	'GET',	7),
(12,	'POST',	7),
(13,	'GET',	8),
(14,	'POST',	8),
(15,	'GET',	9),
(16,	'GET',	9),
(17,	'GET',	10),
(18,	'GET',	11),
(19,	'GET',	12),
(20,	'GET',	13),
(21,	'GET', 14),
(22,	'PATCH', 14),
(23,	'GET', 15),
(24,	'GET', 16),
(25,	'PATCH', 16),
(26,	'GET', 17),
(27,	'GET', 18),
(28,	'PATCH', 18),
(29,	'GET', 19),
(30,  'POST', 20),
(31,  'POST', 21);
/*!40000 ALTER TABLE `resources_valid_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulings`
--

DROP TABLE IF EXISTS `schedulings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end` timestamp NULL DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unit_id`),
  CONSTRAINT `schedulings_ibfk_2` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES
(1,'Padrão');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password_hash` char(60) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unit_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(1,'admin','$2y$10$V2sdo90Fgz1qhyjCfza4ku.0qx/bXinGajBDd/6W.xigyUURi4jKa','Administrador',1,3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `views`
--

DROP TABLE IF EXISTS `views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `views` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `html` blob NOT NULL,
  `edit_view_class` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_edit_view_class` (`edit_view_class`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `views`
--

LOCK TABLES `views` WRITE;
/*!40000 ALTER TABLE `views` DISABLE KEYS */;
INSERT INTO `views` VALUES
(1,'<!DOCTYPE html>\n<html>\n   <head>\n     <title>...::: MediaShare :::...</title>\n     <meta charset=\"utf-8\" />\n     <meta name=\"author\" content=\"Edgar Roberto Lemke\" />\n     <link rel=\"stylesheet\" href=\"/MediaShare/static/css/stylesheet.css\" />\n   </head>\n   <body>\n     $content\n     <script type=\"text/javascript\" src=\"/MediaShare/static/js/script.js\"></script>\n     <script type=\"text/javascript\" src=\"/MediaShare/static/js/autocomplete.js\"></script>\n     <script type=\"text/javascript\" src=\"/MediaShare/static/js/grid.js\"></script>\n     <script type=\"text/javascript\" src=\"/MediaShare/static/js/selector.js\"></script>\n   </body>\n</html>',NULL),
(2,'<header>\n  <img class=\"logo\" src=\"/MediaShare/static/img/logo.svg\">\n  <h1>MediaShare</h1>\n  <a href=\"/MediaShare/logout?api_key=$api_key\"><img class=\"exit-button\" src=\"/MediaShare/static/img/exit.png\"></a>\n  <nav class=\"Menu\"></div>\n</header>\n<main>\n$content\n</main>\n<footer class=\"Footer\"></footer>',NULL),
(3,'<header>\n  <img class=\"logo\" src=\"/MediaShare/static/img/logopmjs2018dark.svg\">\n  <h1>MediaShare</h1>\n  <nav class=\"static_menu\"></div>\n</header>\n<main>\n<form action=\"/MediaShare/login?api_key=$api_key\" method=\"POST\">\n    <div class=\"table\" id=\"login\">\n         <div class=\"table_row\">\n           <div class=\"table_cell\">Login</div>\n           <div class=\"table_cell\"><input type=\"text\" name=\"username\" /></div>\n         </div>\n         <div class=\"table_row\">\n           <div class=\"table_cell\">Senha</div>\n           <div class=\"table_cell\"><input type=\"password\" name=\"password\" /></div>\n         </div>\n    </div>\n    <br>\n    <input type=\"submit\" value=\"OK\" />\n    <input type=\"hidden\" name=\"api_key\" value=\"$api_key\" />\n</form>\n</main>\n<footer class=\"Footer\"></footer>',NULL),
(4,'Que bom que você voltou para o MediaShare!',NULL),
(5,'<form method=\"PATCH\" action=\"/MediaShare/players_config/1?api_key=$api_key\" onsubmit=\"update_resource( this ); return false;\">\nLease time: <input type=\"number\" name=\"lease_time\">&nbsp;minutos&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Atualizar\">\n</form>\n<br>\n<div class=\"Grid\" data-table=\"players\" data-search-uri=\"/MediaShare/players/search?api_key=$api_key\"></div>',NULL),
(6,'<form method=\"POST\" onsubmit=\"MediaFiles.upload_files( this ); return false;\">\n  Arquivo de mídia:\n  <input type=\"file\" name=\"files_selector\" accept=\".mp4\" multiple>\n  <input type=\"submit\" value=\"Enviar\">\n</form>\n<br>\n<div class=\"Grid\" data-table=\"media_files\" data-search-uri=\"/MediaShare/media_files/search?api_key=$api_key\"></div>',NULL),
(7,'<input type=\"button\" value=\"Cadastrar Grupo de Mídia\" onclick=\"open_register_screen( \'/MediaShare/media_groups/register_screen\', \'Cadastrar Grupo de Mídia\' );\">\n<br>\n<div class=\"Grid\" data-table=\"media_groups\" data-search-uri=\"/MediaShare/media_groups/search?api_key=$api_key\"></div>',NULL),
(8,'<input type=\"button\" value=\"Cadastrar Agendamento\" onclick=\"open_register_screen( \'/MediaShare/schedulings/register_screen\', \'Cadastrar Agendamento de Reprodução\') ;\">\n<br>\n<div class=\"Grid\" data-table=\"schedulings\" data-search-uri=\"/MediaShare/schedulings/search?api_key=$api_key\"></div>',NULL),
(9,'<input type=\"button\" value=\"Cadastrar Usuário\" onclick=\"open_register_screen( \'/MediaShare/users/register_screen\', \'Cadastrar Usuário\' );\"> \n<br>\n<div class=\"Grid\" data-table=\"users\" data-search-uri=\"/MediaShare/users/search?api_key=$api_key\"></div>',NULL),
(10,'<input type=\"button\" value=\"Cadastrar Unidade\" onclick=\"open_register_screen( \'/MediaShare/units/register_screen\', \'Cadastrar Unidade\' );\">\n<br>\n<div class=\"Grid\" data-table=\"units\" data-search-uri=\"/MediaShare/units/search?api_key=$api_key\"></div>',NULL),
(11,'<form method=\"POST\" action=\"/MediaShare/units?api_key=$api_key\" onsubmit=\"post_form( this ); return false;\">\n  <table>\n    <tbody>\n      <tr>\n        <td>Nome</td>\n        <td><input type=\"text\" name=\"name\" maxlength=\"255\"></td>\n      </tr>\n    </tbody>\n  </table>\n  <input type=\"submit\" value=\"Cadastrar\">\n</form>',NULL),
(12,'<form method=\"POST\" action=\"/MediaShare/users?api_key=$api_key\" onsubmit=\"post_form( this ); return false;\">\n  <table>\n    <tbody>\n      <tr>\n        <td>Nome</td>\n        <td><input type=\"text\" name=\"name\" maxlength=\"255\"></td>\n      </tr>\n      <tr>\n        <td>Login</td>\n        <td><input type=\"text\" name=\"username\" maxlength=\"255\"></td>\n      </tr>\n      <div class=\"PasswordFieldsPair\">\n        <tr>\n          <td>Senha</td>\n          <td><input type=\"password\" name=\"password\" maxlength=\"64\" class=\"PasswordField\" onchange=\"Password.validate();\"></td>\n        </tr>\n        <tr>\n          <td>Confirmar senha</td>\n          <td><input type=\"password\" name=\"confirm_password\" maxlength=\"64\" class=\"PasswordField\" onchange=\"Password.validate();\"></td>\n        </tr>\n      </div>\n      <tr>\n        <td>Unidade</td>\n        <td><input type=\"text\" class=\"AutoComplete\" name=\"unit_name\" data-table=\"units\" data-column=\"name\" data-hidden-name=\"unit_id\"></td>\n      </tr>\n      <tr>\n        <td>Permissão</td>\n        <td>\n          <select name=\"permission\">\n            <option value=\"0\">Inativo</option>\n            <option value=\"1\">Usuário Local</option>\n            <option value=\"2\">Gerente</option>\n            <option value=\"3\">Administrador</option>\n          </select>\n        </td>\n      </tr>\n    </tbody>\n  </table>\n  <input type=\"submit\" value=\"Cadastrar\">\n</form>',NULL),
(13,'<form method=\"POST\" action=\"/MediaShare/schedulings?api_key=$api_key\" onsubmit=\"post_form( this ); return false;\">\n  <table>\n    <tbody>\n\n      <tr>\n        <td>Início</td>\n        <td><input type=\"date\" name=\"start_date\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\">&nbsp;<input type=\"time\" name=\"start_time\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\"></td>\n      </tr>\n\n      <tr>\n        <td>Fim</td>\n        <td><input type=\"date\" name=\"end_date\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\">&nbsp;<input type=\"time\" name=\"end_time\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\"></td>\n      </tr>\n\n      <tr>\n        <td>Unidade</td>\n        <td><input type=\"text\" class=\"AutoComplete\" name=\"unit_name\" maxlength=\"64\" data-table=\"units\" data-column=\"name\" data-hidden-name=\"unit_id\"></td>\n      </tr>\n\n    </tbody>\n  </table>\n\n  <div class=\"Selector\" data-table=\"media_groups\" data-column=\"name\" data-column-label=\"Grupo de Mídia\" data-relationship-table=\"media_groups_schedulings\" data-relationship-column=\"unit_id\"></div>\n\n  <input type=\"submit\" value=\"Cadastrar\">\n</form>',NULL),
(14,'<form method=\"POST\" action=\"/MediaShare/media_groups?api_key=$api_key\" onsubmit=\"post_form( this ); return false;\">\n  <table>\n    <tbody>\n      <tr>\n        <td>Nome</td>\n        <td><input type=\"text\" name=\"name\" maxlength=\"255\"></td>\n      </tr>\n    </tbody>\n  </table>\n  <br>\n  <div class=\"Selector\" data-table=\"media_files\" data-column=\"path\" data-column-label=\"Arquivo\" data-relationship-table=\"media_groups_media_files\" data-relationship-column=\"media_file_id\"></div>\n  <br>\n  <input type=\"submit\" value=\"Cadastrar\">\n</form>',NULL),
(15,'<form method=\"PATCH\" data-table=\"units\" onsubmit=\"update_resource( this ); return false;\">\n  <table>\n    <tbody>\n      <tr>\n        <td>ID</td>\n        <td><input type=\"text\" name=\"id\" maxlength=\"32\" readonly></td>\n      </tr>\n      <tr>\n        <td>Nome</td>\n        <td><input type=\"text\" name=\"name\" maxlength=\"255\"></td>\n      </tr>\n    </tbody>\n  </table>\n  <input type=\"submit\" value=\"Atualizar\">\n</form>','units'),
(16,'<form method=\"PATCH\" data-table=\"users\" onsubmit=\"update_resource( this ); return false;\">\n  <table>\n    <tbody>\n      <tr>\n        <td>ID</td>\n        <td><input type=\"text\" name=\"id\" maxlength=\"32\" readonly></td>\n      </tr>\n      <tr>\n        <td>Nome</td>\n        <td><input type=\"text\" name=\"name\" maxlength=\"255\"></td>\n      </tr>\n      <tr>\n        <td>Login</td>\n        <td><input type=\"text\" name=\"username\" maxlength=\"255\"></td>\n      </tr>\n      <div class=\"PasswordFieldsPair\">\n        <tr>\n          <td>Senha</td>\n          <td><input type=\"password\" name=\"password\" maxlength=\"64\" class=\"PasswordField\" onchange=\"Password.validate();\"></td>\n        </tr>\n\n        <tr>\n          <td>Confirmar senha</td>\n          <td><input type=\"password\" name=\"confirm_password\" maxlength=\"64\" class=\"PasswordField\" onchange=\"Password.validate();\"></td>\n        </tr>\n      </div>\n      <tr>\n        <td>Unidade</td>\n        <td><input type=\"text\" class=\"AutoComplete\" name=\"unit_name\" data-table=\"units\" data-column=\"name\" data-hidden-name=\"unit_id\" maxlength=\"255\"></td>\n      </tr>\n      <tr>\n        <td>Permissão</td>\n        <td>\n          <select name=\"permission\">\n            <option value=\"0\">Inativo</option>\n            <option value=\"1\">Usuário Local</option>\n            <option value=\"2\">Gerente</option>\n            <option value=\"3\">Administrador</option>\n          </select>\n        </td>\n      </tr>\n    </tbody>\n  </table>\n  <input type=\"submit\" value=\"Atualizar\">\n</form>','users'),
(17,'<form method=\"PATCH\" data-table=\"schedulings\" onsubmit=\"update_resource( this ); return false;\">\n  <table>\n    <tbody>\n\n      <tr>\n        <td>ID</td>\n        <td><input type=\"text\" name=\"id\" maxlength=\"32\" readonly></td>\n      </tr>\n\n      <tr>\n        <td>Início</td>\n        <td><input type=\"date\" name=\"start_date\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\">&nbsp;<input type=\"time\" name=\"start_time\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\"></td>\n      </tr>\n\n      <tr>\n        <td>Fim</td>\n        <td><input type=\"date\" name=\"end_date\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\">&nbsp;<input type=\"time\" name=\"end_time\" onchange=\"Schedulings.format_datetime();\" data-skip-submit=\"\"></td>\n      </tr>\n\n      <tr>\n        <td>Unidade</td>\n        <td><input type=\"text\" class=\"AutoComplete\" name=\"unit_name\" maxlength=\"64\" data-table=\"units\" data-column=\"name\" data-hidden-name=\"unit_id\"></td>\n      </tr>\n\n    </tbody>\n  </table>\n\n  <div class=\"Selector\" data-table=\"media_groups\" data-column=\"name\" data-column-label=\"Grupo de Mídia\" data-relationship-table=\"media_groups_schedulings\" data-relationship-column=\"scheduling_id\"></div>\n\n  <input type=\"submit\" value=\"Atualizar\">\n</form>','schedulings'),
(18,'<form method=\"PATCH\" data-table=\"media_groups\" onsubmit=\"update_resource( this ); return false;\">\n  <table>\n    <tbody>\n      <tr>\n        <td>ID</td>\n        <td><input type=\"text\" name=\"id\" readonly></td>\n      </tr>\n      <tr>\n        <td>Nome</td>\n        <td><input type=\"text\" name=\"name\" maxlength=\"255\"></td>\n      </tr>\n    </tbody>\n  </table>\n  <br>\n  <div class=\"Selector\" data-table=\"media_files\" data-column=\"path\" data-column-label=\"Arquivo\" data-relationship-table=\"media_files_media_groups\" data-relationship-column=\"media_group_id\"></div>\n  <input type=\"submit\" value=\"Atualizar\">\n</form>','media_groups');
/*!40000 ALTER TABLE `views` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-12  9:01:32
