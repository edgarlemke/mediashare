<?php


class Players {


  public static function POST ( $resource, $logged_in ) {

    if ( strpos( $resource[ 'URI' ], 'reboot' ) !== false ) {

      return Players::reboot( $resource );

    }

    if ( strpos( $resource[ 'URI' ], 'screenshot' ) !== false ) {

      return Players::screenshot( $resource );

    }

    extract( $_POST );

    $seed = $hardware . ':' . $serial . ':' . $client_version . ':' . rand();
    $client_key = hash( 'sha256', $seed );

    create(
      'players',
      array(
        'hostname' => $hostname,
        'hardware' => $hardware,
        'serial' => $serial,
        'client_version' => $client_version,
        'client_key' => $client_key,
        'unit_id' => $unit_id
      )
    );

    $results = read(
      'players',
      array( '*' ),
      $conditions= null,
      $sorting= array( 'id' => 'DESC' ),
      $splitting= array( 0, 1 )
    );

    $new_entity = $results[ 0 ];
    $new_entity_id = $new_entity[ 'id' ];

    // trigger_error( 'new_entity_id: ' . $new_entity_id );

    $uri = "${resource[ 'URI' ]}/${new_entity_id}";
    $new_resource_id = create_REST_resource(
      $uri,
      'players',
      $new_entity_id,
      null,
      $logged_in,
      array( 'GET', 'DELETE' )
    );


    $uri = "${resource[ 'URI' ]}/${new_entity_id}/reboot";

    $new_resource_reboot_id = create_REST_resource(
      $uri,
      'players',
      $new_entity_id,
      null,
      $logged_in,
      array( 'POST' )
    );


    $uri = "${resource[ 'URI' ]}/${new_entity_id}/screenshot";

    $new_resource_reboot_id = create_REST_resource(
      $uri,
      'players',
      $new_entity_id,
      null,
      $logged_in,
      array( 'GET' )
    );


    return $client_key;

  }


  public static function DELETE ( $resource, $logged_in ) {

  }


  private static function reboot ( $resource ) {

    trigger_error( 'resource: ' . print_r( $resource, true ) );

    $player_id = $resource[ 'entity_id' ];

    $results = read(
      'players',
      array( '`players_reports`.`ip`' ),
      $conditions = new Condition( 'player_id', '==', $player_id ),
      $sorting= array( 'player_id' => 'DESC' ),
      $splitting= array( 0, 1 ),
      $joins= array(
        new InnerJoin( 'players', 'id', 'players_reports', 'player_id' )
      )
    );

    $ip = $results[ 0 ][ 'ip' ];


    $root = '/var/www/html/MediaShare';
    $ssh_user = 'mediashare';

    // $cmd = "ssh -i $root/ssh/mediashare -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=$root/ssh/known_hosts\" $ssh_user@$ip \"sudo reboot\" ";
    $cmd = "ssh -i $root/ssh/mediashare -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" mediashare@$ip \"echo REBOOTING ; sudo reboot\" ";

    $output_cmd = print_r( shell_exec( $cmd ), true );

    if ( strpos( $output_cmd, 'REBOOTING' ) !== false ) {
      return 'OK';
    }

    else {
      http_response_code( 503 );
      return $output_cmd;
    }

  }


  private static function screenshot ( $resource ) {

    trigger_error( 'resource: ' . print_r( $resource, true ) );

    $player_id = $resource[ 'entity_id' ];

    $results = read(
      'players',
      array( '`players_reports`.`ip`' ),
      $conditions = new Condition( 'player_id', '==', $player_id ),
      $sorting= array( 'player_id' => 'DESC' ),
      $splitting= array( 0, 1 ),
      $joins= array(
        new InnerJoin( 'players', 'id', 'players_reports', 'player_id' )
      )
    );

    $ip = $results[ 0 ][ 'ip' ];


    $root = '/var/www/html/MediaShare';
    $ssh_user = 'mediashare';

    // $cmd = "ssh -i $root/ssh/mediashare -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=$root/ssh/known_hosts\" $ssh_user@$ip \"sudo reboot\" ";
    $cmd = "ssh -i $root/ssh/mediashare -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" mediashare@$ip \"~/mediashare/take_screenshot.bash\" ";
    $output_cmd = shell_exec( $cmd );


    $cmd = "scp -i $root/ssh/mediashare -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" mediashare@$ip:~/mediashare/screenshot.png $root/uploads/screenshots/$player_id.png";
    $output_cmd = shell_exec( $cmd );

    return "uploads/screenshots/$player_id.png";

  }


  public static function GET_screenshot ( $resource, $logged_in ) {

    return Players::screenshot( $resource );

  }


}


?>
