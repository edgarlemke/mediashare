<?php

class Schedulings {

  public static function POST ( $resource, $logged_in ) {

    $start = $_POST[ 'start' ];
    $end = $_POST[ 'end' ];
    $unit_id = $_POST[ 'unit_id' ];

    $Selector_items = $_POST[ 'Selector_items' ];

    $data = array(
      'start' => $start,
      'end' => $end,
      'unit_id' => $unit_id
    );
    create( 'schedulings', $data );

    $results = read(
      'schedulings',
      array( '*' ),
      $conditions= null,
      $sorting= array( 'id' => 'DESC' ),
      $splitting= array( 0, 1 )
    );

    $new_entity = $results[ 0 ];
    $new_entity_id = $new_entity[ 'id' ];


    $uri = "${resource[ 'URI' ]}/${new_entity_id}";
    $new_resource_id = create_REST_resource(
      $uri,
      'schedulings',
      $new_entity_id,
      null,
      $logged_in,
      array( 'GET', 'DELETE', 'PATCH' )
    );

    // trigger_error( 'new_resource_id: ' . $new_resource_id );


    $results = read (
        'views',
        array( 'id' ),
        $conditions = new Condition (
          'edit_view_class', '==', $resource[ 'entity_name' ]
        )
    );
    $edit_view = $results[ 0 ];


    $uri = "${resource[ 'URI' ]}/${new_entity_id}/edit";

    $new_resource_edit_id = create_REST_resource(
      $uri,
      $resource[ 'entity_name' ],
      $new_entity_id,
      $edit_view[ 'id' ],
      $logged_in,
      array( 'GET' )
    );


    // trigger_error( 'Selector items: ' . print_r( $Selector_items, true ) );
    foreach ( $Selector_items as $key => $media_group_id )  {

      $data = array(
        'media_group_id' => $media_group_id,
        'scheduling_id' => $new_entity_id
      );

      create( 'media_groups_schedulings', $data );

    }

    return '';


  }

  public static function PATCH ( $resource, $logged_in, $parsed )  {

    trigger_error( 'parsed: ' . print_r( $parsed, true ) );

    $Selector_items = $parsed[ 'Selector_items' ];
    unset( $parsed[ 'Selector_items' ] );

    update(
        $resource[ 'entity_name' ],
        $parsed,
        new Condition( 'id', '==', $resource[ 'entity_id' ] )
    );


    /* track changes in files */
    $item_rows = read(
      'media_groups_schedulings',
      array( 'media_group_id' ),
      $conditions= new Condition( 'scheduling_id', '==', $resource[ 'entity_id' ] )
    );

    $received_items_ids = array();
    foreach( $Selector_items as $key => $item_id ) {
      array_push( $received_items_ids, $item_id );
    }

    $stored_items_ids = array();
    foreach( $item_rows as $key => $row ) {
      array_push( $stored_items_ids, $row[ 'media_group_id' ] );
    }
    /* ----- x ----- */

    trigger_error( print_r( $stored_items_ids, true ) );
    trigger_error( print_r( $received_items_ids, true ) );


    /* effect changes in files */
    $to_create = array_diff( $received_items_ids, $stored_items_ids );
    trigger_error( 'to_create: ' . print_r( $to_create, true ) );
    foreach( $to_create as $key => $item_id ) {

      $data = array(
        'media_group_id' => $item_id,
        'scheduling_id' => $resource[ 'entity_id' ]
      );
      create( 'media_groups_schedulings', $data );

    }

    $to_delete = array_diff( $stored_items_ids, $received_items_ids );
    trigger_error( 'to_delete: ' . print_r( $to_delete, true ) );
    foreach( $to_delete as $key => $item_id ) {

      delete(
        'media_groups_schedulings',
        new Condition(
            new Condition( 'media_group_id', '==', $item_id ),
            'AND',
            new Condition( 'scheduling_id', '==', $resource[ 'entity_id' ] )
        )
      );

    }
    /* ----- x ----- */


    $rows = read(
        $resource[ 'entity_name' ],
        array( '*' ),
        $conditions= new Condition( 'id', '==', $resource[ 'entity_id' ] ),
        $sorting= array( 'id' => 'DESC' ),
        $splitting= [ 0, 1 ]
    );
    $new_entity = $rows[ 0 ];

    return forge_entity_table( $new_entity, $logged_in );

  }

  public static function DELETE ( $resource, $logged_in ) {

    $scheduling_id = $resource[ 'entity_id' ];

    delete(
      'media_groups_schedulings',
      new Condition( 'scheduling_id', '==', $scheduling_id )
    );

    delete(
      'schedulings',
      new Condition( 'id', '==', $scheduling_id )
    );


    // Apaga métodos válidos para URI da tabela resources_valid_methods
    $condition = new Condition( 'resource_id', '==', $resource[ 'id' ] );
    delete( 'resources_valid_methods', $conditions= $condition );

    // Apaga URI da tabela resources
    $condition = new Condition( 'id', '==', $resource[ 'id' ] );
    delete( 'resources', $conditions= $condition );

    http_response_code( 204 );
    exit();

  }

}

?>
