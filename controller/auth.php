<?php

$PERM_ADMIN = 3;
$PERM_MANAGER = 2;
$PERM_LOCAL_USER = 1;
$PERM_DISABLED = 0;

$permission_level = array(

  'players_config'  => $PERM_ADMIN,
  'media_files'     => $PERM_LOCAL_USER,
  'media_groups'    => $PERM_MANAGER,
  'users'           => $PERM_ADMIN,
  'units'           => $PERM_ADMIN,
  'schedulings'     => $PERM_LOCAL_USER

);

// trigger_error( 'auth.php loaded' );

// Funções de senha

// Verifica se senha de usuário é válida
function check_password ( $username, $password ) {

  $conditions = new Condition( 'username', '==', $username );

  $results = read( 'users', array( 'password_hash' ), $conditions );

  if( ! $results )  {
    // throw new Exception ( "Couldn't retrieve password hash from database" );
    return false;
  }

  $password_hash = $results[ 0 ][ 'password_hash' ];

  return password_verify( $password, $password_hash );

}

// Efetua hashing em senha
function protect_password ( $password ) {

  return password_hash( $password, PASSWORD_DEFAULT );

}


function check_client_key( $client_key )  {

  $conditions = new Condition( 'client_key', '==', $client_key );

  $results = read(
    'players',
    array( 'client_key' ),
    $conditions
  );

  return isset( $results[ 0 ][ 'client_key' ] );

}


?>
