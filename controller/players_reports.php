<?php

class PlayersReports {


  public static function POST ( $resource, $logged_in ) {

    // Create new entity from POST data
    $created = create( $resource[ 'entity_name' ], $_POST );

    $rows = read (
        $resource[ 'entity_name' ],
        array( '*' ),
        $conditions= null,
        $sorting= array( 'id' => 'DESC' ),
        $splitting= [ 0, 1 ]
    );

    $new_entity = $rows[ 0 ];
    $new_entity_id = $new_entity[ 'id' ];

    return 'OK';

  }


}

?>
