<?php

/*

ARQUIVO config.php

É responsável por guardar as informações de acesso à base de dados usadas pelo
Model ( arquivo model.php ), que utiliza a classe PDO.

*/

$PDO_config = array(

  'host' => 'localhost' ,
  'port' => 3306 ,
  'username' => 'mediashare' ,
  'password' => 'PASSWORD_GOES_HERE' ,
  'database' => 'mediashare'

)

$public_root = '/';

?>
