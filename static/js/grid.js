function Grid ( div ) {

  this.id = Grid.objects.length;
  console.log( 'New Grid ID:' + this.id );
  Grid.objects.push( this );

  this.div = div;

  if ( ! div.hasAttribute( 'data-table' ) )  {
    console.log( div );
    throw new 'Grid DIV has no data-table attribute';
  }
  this.table = div.getAttribute( 'data-table' );

  // TODO: tirar data-search-uri, criar URI a partir da data-table
  if ( ! div.hasAttribute( 'data-search-uri' ) )  {
    console.log( div );
    throw new 'Grid DIV has no data-search-uri attribute';
  }
  this.search_uri = div.getAttribute( 'data-search-uri' );

  // this.input é setado nesta função
  this.fix_search_inputs();

  this.tbody = div.getElementsByTagName( 'tbody' )[ 0 ];

  Grid.search( this.id );

}
/* Atributos e métodos estáticos */
Grid.objects = [];
Grid.onload = function () {

    var grid_divs = document.getElementsByClassName( 'Grid' );
    for ( var c = 0 ; c < grid_divs.length ; c++ ) {

      var grid_div = grid_divs[ c ];

      var grid_obj = new Grid( grid_div );


    }

}
Grid.search = function ( object_id ) {

  var obj = Grid.objects[ object_id ];

  var data = '';
  for ( var c = 0; c < obj.inputs.length; c++ )  {

    var input = obj.inputs[ c ];
    // console.log( input );

    // Pula inputs vazios ou sem nome
    if ( input.name.length <= 0 || input.value.length <= 0 || input.value == '' ) {
      continue;
    }

    var data = data + '&' + input.name + '=' + encodeURIComponent( input.value );
    // console.log( 'data: ' + data );

  }
  for ( var c = 0; c < obj.selects.length; c++ )  {

    var select = obj.selects[ c ];

    if ( select.selectedIndex == 0 )  {
      continue;
    }

    var data = data + '&' + select.name + '=' + select.selectedOptions[ 0 ].value;

  }


  var uri = obj.search_uri + data;
  console.log( uri );

  var handler = (
    function ( obj ) {

      return function ( xhr )  {

        if ( xhr.status == 200 )  {

          // console.log( xhr.responseText );

          var parser = new DOMParser();
          var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );

          var rows = xmldoc.getElementsByTagName( 'row' );
          // console.log( rows );

          var tbody = obj.div.getElementsByTagName( 'tbody' )[ 0 ];

          // Limpa conteúdo antigo
          while ( tbody.firstChild )
            tbody.removeChild( tbody.firstChild );


          for ( var c = 0; c < rows.length; c++ ) {

            var row = rows[ c ];
            // console.log( row );

            var tr = document.createElement( 'tr' );
            tbody.appendChild( tr );

            var columns = row.children;
            var row_id = null;
            for ( var c2 = 0; c2 < columns.length; c2++ ) {

              var column = columns[ c2 ];
              // console.log( column );

              if ( column.tagName == 'id' ) {
                var row_id = column.textContent;
              }

              var td = document.createElement( 'td' );
              tr.appendChild( td );

              var textContent = column.textContent;

              try {

                var dt = new Date( textContent );

                if( isNaN( dt.getTime() ) ) {
                  throw '';
                }

                var textContent = ( '00' + dt.getDate() ).slice( -2 ) + '/' +
                                  ( '00' + ( dt.getMonth() + 1 ) ).slice( -2 ) + '/' +
                                  ( dt.getFullYear() ) + ' ' +
                                  ( '00' + dt.getHours() ).slice( -2 ) + ':' +
                                  ( '00' + dt.getMinutes() ).slice( -2 );
                                  // + ':' + ( '00' + dt.getSeconds() ).slice( -2 );

              }
              catch( e )  {}

              var text = document.createTextNode( textContent );

              if ( obj.table == 'media_files' && column.tagName == 'path' ) {

                var a = document.createElement( 'a' );
                a.setAttribute( 'href', '/MediaShare/uploads/' + textContent );

                a.appendChild( text );

                td.appendChild( a );

              }
              else if ( obj.table == 'users' && column.tagName == 'permission' )  {

                var textContent = Users.permissions[ Number( textContent ) ];
                var text = document.createTextNode( textContent );
                td.appendChild( text );

              }
              else {

                td.appendChild( text );

              }


            }


            var actions_td = document.createElement( 'td' );


            if ( obj.table == 'media_files' ) {

              actions_td.innerHTML = '<div onclick="open_delete_screen( \'' + row_id + '\' );" style="display: inline;"><img class="inline-icon" src="/MediaShare/static/img/delete.png"></div>';

            }
            else if ( obj.table == 'players' ) {

              actions_td.innerHTML = '<img class="inline-icon" src="/MediaShare/static/img/camera.png" onclick="Grid.Players.take_screenshot( \'' + row_id + '\' );">&nbsp;<img class="inline-icon" src="/MediaShare/static/img/reboot.png" onclick="Grid.Players.reboot( \'' + row_id + '\' );">&nbsp;<img class="inline-icon" src="/MediaShare/static/img/delete.png" onclick="open_delete_screen( \'' + row_id + '\' );" style="display: inline;">';

            }
            else {

              actions_td.innerHTML = '<div onclick="open_edit_screen( \'' + row_id + '\', \'' + obj.table + '\' );" style="display: inline;"><img class="inline-icon" src="/MediaShare/static/img/edit.png"></div>&nbsp;<div onclick="open_delete_screen( \'' + row_id + '\' );" style="display: inline;"><img class="inline-icon" src="/MediaShare/static/img/delete.png"></div>';

            }

            tr.appendChild( actions_td );

          }

        }
        else {
          console.log( xhr );
        }

      }

    }
  )( obj );

  request( 'GET', uri, handler );

}

/* Métodos do protótipo */
Grid.prototype.fix_search_inputs = function () {

  var thead = this.div.getElementsByTagName( 'thead' )[ 0 ];
  this.inputs = thead.getElementsByTagName( 'input' );

  for ( var c = 0; c < this.inputs.length; c++ ) {

    var input = this.inputs[ c ];
    input.setAttribute( 'onkeyup', 'Grid.search( ' + this.id + ')' );

  }

  this.selects = thead.getElementsByTagName( 'select' );
  for ( var c = 0; c < this.selects.length; c++ )  {

    var select = this.selects[ c ];
    select.setAttribute( 'onchange', 'Grid.search( ' + this.id + ')' );

  }


}

Grid.onload();


Grid.Players = {};
Grid.Players.reboot = function ( row_id ) {

  var uri = '/MediaShare/players/' + row_id + '/reboot?api_key=' + key;

  var handler = function ( xhr )  {

    if ( xhr.status == 200 )  {

      new Popup( 'Sucesso', 'Comando para reiniciar enviado. O cliente deve estar on-line novamente em poucos minutos.', function(){ window.location.reload() } );

    }
    else {

      new Popup( 'Erro', 'Não foi possível enviar comando para reiniciar.', function(){ window.location.reload() } );

    }

  }

  request( 'POST', uri, handler );

}
Grid.Players.take_screenshot = function ( row_id )  {

  var uri = '/MediaShare/players/' + row_id + '/screenshot?api_key=' + key;

  var handler = function ( xhr )  {

    if ( xhr.status == 200 )  {

      var src = '/MediaShare/' + xhr.responseText;

      new Popup( 'Sucesso', 'Screenshot:<br><img src="' + src + '">', function(){ window.location.reload() } );

    }
    else {

      new Popup( 'Erro', 'Não foi possível enviar comando para tirar screenshot.', function(){ window.location.reload() } );

    }

  }

  request( 'GET', uri, handler );

}
