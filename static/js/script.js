url = new URL( window.location.href );
// console.log( url );

key = url.searchParams.get( 'api_key' );


function request ( method, uri, handler, data, headers )  {

  var xhr = new XMLHttpRequest();

  xhr.onload = ( function ( handler ) {
    return function() {
      if ( xhr.readyState == 4 ) {
        handler( xhr );
      }
    }
  } ) ( handler );

  // Abre request
  xhr.open( method, uri, true );

  // Define MIME de formulário para envio
  if ( data !== undefined )
    xhr.setRequestHeader( 'Content-type', 'application/x-www-form-urlencoded' );

  // Define headers do request
  for( key in headers ) {
    xhr.setRequestHeader( key, headers[ key ] );
  }

  // Se data foi fornecido, enviar requisição com dados
  if ( data !== undefined ) {
    console.log( data );
    xhr.send( data );
  }
  // Se não, apenas envia requisição
  else {
    xhr.send();
  }

}


function open_register_screen ( uri, popup_title ) {

  var handler = function ( xhr ) {

    if ( xhr.status == 200 ) {

      var form =  xhr.responseText;

      var popup = new Popup( popup_title, form );

      Selector.load();

    }
    else {
      console.log( xhr.status );
    }

  }

  request( 'GET', uri + '?api_key=' + key, handler );

}

function post_form ( form ) {

  // MediaFilesSelector.fix_inputs();
  Selector.fix_inputs();

  var data = extract_form_data( form );

  var handler = function ( xhr )  {

    if ( xhr.status == 200 )  {

      // var text = xhr.responseText;
      var text = 'Recurso criado com sucesso.';

      var popup = new Popup( 'Sucesso', text, function(){ window.location.reload() } );

    }
    else {
      console.log( xhr.status );
    }

  }

  request( 'POST', form.action, handler, data );

}

function extract_form_data ( form ) {

  var data = '';
  var elements = form.elements;
  // console.log( elements );

  for ( var c = 0; c < elements.length; c++ ) {

    var element = elements[ c ];
    // console.log( element );

    if ( element.getAttribute( 'class' ) == 'AutoComplete' )  {
      continue;
    }

    var value = element.value;
    // console.log( value );

    if ( ! element.hasAttribute( 'name' ) ) {
      // console.log( 'skipping element without name' );
      continue;
    }
    var name = element.getAttribute( 'name' );


    if ( name == 'confirm_password' ) {
      continue;
    }

    if ( element.hasAttribute( 'data-skip-submit' ) ) {
      continue;
    }


    if ( value === null || value === undefined || value == '' )  {
      continue;
    }

    if( data.length ) {
      // console.log( 'old_data: ' + data );
      var data = data + '&';
    }
    var data = data + encodeURIComponent( name ) + '=' + encodeURIComponent( value );

  }

  return data;

}


function open_delete_screen ( resource_id ) {

  var uri = url.pathname + '/' + resource_id + '?api_key=' + key ;

  var body_html = 'Tem certeza que deseja excluir?<br>\
  <div onclick="delete_resource( \'' + uri + '\' );" style="display: inline;">Sim</div>\
  &nbsp;\
  <div style="display: inline;" onclick="window.location.reload();">Não</div>';

  new Popup ( 'Confirmar exclusão', body_html );

}

function delete_resource ( uri, element ) {

  var handler = function ( xhr ) {

    if ( xhr.status == 204 )  {

      new Popup(
        'Sucesso',
        'Recurso excluído com sucesso.',
        function(){ window.location.reload(); }
      );

    }
    else {

      new Popup(
        'Erro',
        'Não foi possível excluir recurso. Por favor tente novamente mais\
        tarde ou contate o administrador do sistema.',

        function(){ window.location.reload(); }
      );

    }

  }

  request( 'DELETE', uri, handler );

}


function open_edit_screen ( resource_id, table ) {

  // console.log( url );

  // TODO: consertar - Usando url.pathname o Grid fica restrito a usar sempre a collection/resource da URL
  var uri = url.pathname + '/' + resource_id + '/edit?api_key=' + key
  console.log( 'open_edit_screen uri: ' + uri );

  var handler = function ( xhr ) {

    if ( xhr.status == 200 ) {

      var form_text =  xhr.responseText;
      // console.log( form_text );

      var parser = new DOMParser();
      var doc = parser.parseFromString( form_text, "text/html" );

      // console.log( doc );

      var forms = doc.getElementsByTagName( 'form' );
      var form = forms[ 0 ];

      var action = url.pathname + '/' + resource_id + '?api_key=' + key;
      form.setAttribute( 'action', action );

      // console.log( form );


      /* fix select/options selected value */
      var selects = form.getElementsByTagName( 'select' );
      for ( var c = 0; c < selects.length; c++ )  {

        var each_select = selects[ c ];

        if ( ! each_select.hasAttribute( 'value' ) ) {
          continue;
        }

        var select_value = each_select.getAttribute( 'value' );

        var options = each_select.getElementsByTagName( 'option' );

        for ( var d = 0; d < options.length; d++ )  {

          var each_option = options[ d ];

          if ( ! each_option.hasAttribute( 'value' ) )  {
            continue;
          }

          var option_value = each_option.getAttribute( 'value' );

          if ( select_value == option_value ) {

            each_option.setAttribute( 'selected', 'true' );
            break;

          }

        }

      }
      /* ---- x ---- */


      var popup = new Popup( 'Editar', form.outerHTML );


      AutoComplete.load();
      // MediaFilesSelector.AutoComplete.load();
      Selector.load();


    }
    else {
      console.log( xhr.status );
    }

  }

  request( 'GET', uri, handler );

}

function update_resource ( form ) {

  // MediaFiles\n.fix_inputs();
  Selector.fix_inputs();

  var data = extract_form_data( form );

  // console.log( data );

  var handler = function ( xhr )  {

    if ( xhr.status == 200 )  {

      // var text = xhr.responseText;
      var text = 'Recurso atualizado com sucesso.';

      var popup = new Popup(
        'Sucesso',
        text,
        function(){ window.location.reload() }
      );

    }
    else {

      new Popup(
        'Erro',
        'Não foi possível atualizar recurso. Por favor tente novamente mais\
        tarde ou contate o administrador do sistema.',

        function(){ window.location.reload(); }
      );

    }

  }

  request( 'PATCH', form.action, handler, data );

}


Popup = function ( title_html, body_html, onclose_func ) {

  this.id = Popup.objects.length;
  Popup.objects.push( this );


  this.title_html = title_html;
  this.body_html = body_html;

  if ( typeof onclose_func === 'function' )  {
      this.onclose = onclose_func;
  }
  // else {
  //   console.log( typeof onclose_func );
  // }


  var z_index = 20 + ( this.id * 10 );

  var div_wrapper = document.createElement( 'div' );
  div_wrapper.innerHTML = '<div class="popup_wrapper" style="z-index: ' + z_index + ';">\
  <div class="popup table" data-id="' + this.id + '">\
    <div class="popup_head table_head">\
      <div class="table_row">\
        <div class="table_cell popup_title_cell">\
          <div class="popup_title">' + title_html + '</div>\
          <div class="popup_close_button" onclick="Popup.close(' + this.id + ')"></div>\
        </div>\
      </div>\
    </div>\
    <div class="popup_body table_body">\
      <div class="table_row">\
        <div class="table_cell">' + body_html + '</div>\
      </div>\
    </div>\
  </div>\
  </div>'.trim();

  window.document.body.appendChild( div_wrapper );

}

Popup.objects = [];

Popup.close = function ( popup_id ) {

  // console.log( popup_id );

  var xpath = "//div[contains(@class, 'popup') and @data-id='" + popup_id + "']";
  // console.log( xpath );

  var div_wrapper = getElementByXpath( xpath ).parentElement;

  Popup.objects[ popup_id ].onclose();

  // console.log( div_popup );
  div_wrapper.parentElement.removeChild( div_wrapper );

  Popup.objects[ popup_id ] = null;

}

Popup.prototype.onclose = function () { console.log( "No onclose for Popup." ); }

function getElementByXpath ( path, contextNode ) {

  if ( contextNode == undefined ) {
    var contextNode = document;
  }

  return document.evaluate ( path, contextNode, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;

}


// Two Password Fields Checker
Password = {}
Password.validate = function () {

  var fields = Password.get_fields();

  var field0 = fields[ 0 ];
  var field1 = fields[ 1 ];

  if (    field0.value.length > 0
          && field1.value.length > 0
          && field0.value != field1.value    ) {

    // var custom_validity = "Typed passwords are different from each other. Please fill the same password.";
    var custom_validity = "Senhas digitadas são diferentes. Por favor digite a mesma senha.";

  }
  else {

    var custom_validity = '';

  }

  field0.setCustomValidity( custom_validity );
  field1.setCustomValidity( custom_validity );

}

Password.get_fields = function () {

  var password_field = document.getElementsByClassName( 'PasswordField' )[ 0 ];
  var confirm_password_field = document.getElementsByClassName( 'PasswordField' )[ 1 ];

  return [ password_field, confirm_password_field ];

}


Schedulings = {}
Schedulings.format_datetime = function ()  {

  var start_date_field = document.getElementsByName( 'start_date' )[ 0 ];
  var start_time_field = document.getElementsByName( 'start_time' )[ 0 ];

  var end_date_field = document.getElementsByName( 'end_date' )[ 0 ];
  var end_time_field = document.getElementsByName( 'end_time' )[ 0 ];

  var all_fields = [ start_date_field, start_time_field, end_date_field, end_time_field ];


  var start_date_obj = Schedulings.get_date( start_date_field, start_time_field );

  if ( ! start_date_obj ) {
    return false;
  }

  if ( start_date_obj <= Date.now() )  {

    var func = function ( field ) {

      // field.setCustomValidity( 'Start date and time must be in future.' );
      field.setCustomValidity( 'Data e hora de início deve estar no futuro.' );

    }

    Schedulings.iter_fields( [ start_date_field, start_time_field ], func );

    return false;

  }
  else {

    var func = function ( field ) {

      field.setCustomValidity( '' );

    }

    Schedulings.iter_fields( [ start_date_field, start_time_field ], func );


  }


  var end_date_obj = Schedulings.get_date( end_date_field, end_time_field );

  if ( ! end_date_obj ) {
    return false;
  }


  if ( end_date_obj <= start_date_obj ) {

    var func = function ( field ) {

      // field.setCustomValidity( 'End date and time must be after start date and time.' );
      field.setCustomValidity( 'Data e hora de fim deve estar depois da data e hora de início.' );

    }

    Schedulings.iter_fields( all_fields, func );

    return false;

  }
  else {

    var func = function ( field ) {

      field.setCustomValidity( '' );

    }

    Schedulings.iter_fields( all_fields, func );

  }


  Schedulings.fix_hidden_input( 'start', start_date_field.parentElement, start_date_obj );
  Schedulings.fix_hidden_input( 'end', end_date_field.parentElement, end_date_obj );

}

Schedulings.get_date = function ( date_field, time_field )  {

    if ( ( ! date_field.value ) || ( ! time_field.value ) ) {

      return false;

    }

    var datetime_str = date_field.value + ' ' + time_field.value;
    var datetime = new Date( datetime_str );

    return datetime;

}

Schedulings.iter_fields = function ( _fields, func ) {

  for ( var c = 0; c < _fields.length; c++ ) {

    var each_field = _fields[ c ];

    func( each_field );

  }

}

Schedulings.fix_hidden_input = function ( hidden_field_name, parentElement, datetime ) {

  var xpath = '//input[@type="hidden" and @name="' + hidden_field_name + '"]';
  var hidden_input = getElementByXpath( xpath );

  if ( ! hidden_input ) {

    var hidden_input = document.createElement( 'input' );
    hidden_input.setAttribute( 'type', 'hidden' );
    hidden_input.setAttribute( 'name', hidden_field_name );

    parentElement.appendChild( hidden_input );

  }

  var timezone_offset = ( new Date() ).getTimezoneOffset() * 60000;
  // console.log( timezone_offset );

  var offset_datetime = new Date( datetime - timezone_offset );

  // console.log( offset_datetime );

  hidden_input.setAttribute( 'value', offset_datetime.toISOString().slice( 0, 19 ).replace( 'T', ' ' ) );

}


MediaFiles = {}

MediaFiles.MAX_FILE_SIZE = 250 * 1024 * 1024; // 250 MB

MediaFiles.upload_files = function ( form ) {

  var files_selector = form.elements[ 0 ];

  // console.log( files_selector );

  var files = files_selector.files;

  // console.log( files );

  var template = '<table>' +
  '    <thead>' +
  '        <tr><th>Arquivo</th><th>Status</th></tr>' +
  '    </thead>' +
  '    <tbody>' +
  '    </tbody>' +
  '</table>';
  var doc = new DOMParser().parseFromString( template, 'text/html' );

  var table = doc.getElementsByTagName( 'table' )[ 0 ];

  var tbody = doc.getElementsByTagName( 'tbody' )[ 0 ];
  // console.log( tbody );

  for ( var c = 0; c < files.length; c++ )  {

    var file = files[ c ];
    // console.log( file.name );

    var tr = '<tr><td>' + file.name + '</td><td><progress id="MediaFiles-progress-'+ file.name +'" value="0" max="100"></progress></td></tr>';

    tbody.innerHTML = tbody.innerHTML + tr;

  }

  // console.log( table );

  var popup = new Popup( 'Uploads', table.outerHTML, function(){ window.location.reload() } );

  for ( var c = 0; c < files.length; c++ )  {

    var file = files[ c ];

    MediaFiles.upload_each_file( file );

  }

  return false;

}

MediaFiles.upload_each_file = function ( file )  {

  var pattern = new RegExp( /[^a-zA-Z0-9.]/g );
  var filename = file.name.replace( pattern, '_' );

  if ( file.size > MediaFiles.MAX_FILE_SIZE )  {
    throw 'Arquivo excede limite de tamanho de 250 MB.';
  }

  var xhr = new XMLHttpRequest();
  var fd = new FormData();
  fd.append( 'filename', file, file.name );

  xhr.upload.addEventListener( 'progress', function( e ) {

    if ( e.lengthComputable ) {

      var percentage = Math.round( ( e.loaded * 100 ) / e.total );
      // console.log( percentage );

      var progress = document.getElementById( 'MediaFiles-progress-' + file.name );
      // console.log( progress );

      progress.value = percentage;

    }

  }, false);

  // xhr.onreadystatechange = function ( xhr ) {
  //
  //   if( xhr.readyState == 4 ) {
  //     console.log( xhr );
  //   }
  //   console.log( 'change! ' + xhr.readyState );
  //
  // }

  xhr.addEventListener( 'load', function ( e ) {

    console.log( e );
    console.log( e.target );

    var status = e.target.status;
    var readyState = e.target.readyState;

    if ( readyState == 4 )  {

      var responseText = e.target.responseText;

      // console.log( responseText );

      var progress = document.getElementById( 'MediaFiles-progress-' + file.name );
      // console.log( progress );

      var parent = progress.parentElement;
      parent.removeChild( progress );

      var img = document.createElement( 'img' );

      switch( status )  {
/*
        case 409:
          new Popup( 'Erro', responseText );
          return null;
          break;

        case 413:
          new Popup( 'Erro', responseText );
          return null;
          break;
*/

        case 200:

          img.setAttribute( 'src', 'static/img/success.png' );

          break;

        default:

          img.setAttribute( 'src', 'static/img/error.png' );

          new Popup( 'Erro', responseText );

          break;


      }

      img.setAttribute( 'class', 'inline-icon' );
      parent.appendChild( img );

      return null;

    }

  });

  xhr.open( 'POST', url, true );


  xhr.send( fd );

  // console.log( xhr );

}

// MediaFiles.upload_handler = function ( xhr ) {
//
//   console.log( 'hoi' );
//   console.log( xhr );
//
//   if( xhr.status == 200 ) {
//
//     alert( 'topzera' );
//
//   }
//
// }

Users = {};
Users.permissions = {
  0 : 'Inativo',
  1 : 'Usuário Local',
  2 : 'Gerente',
  3 : 'Administrador'
}


// MediaFilesSelector = {};
//
// MediaFilesSelector.remove_file = function( remove_button ) {
//
//   var tr = remove_button.parentElement.parentElement;
//   var file_id = Number( tr.getAttribute( 'data-file-id', tr ) );
//
//   tr.parentElement.removeChild( tr );
//
//   var index = MediaFilesSelector.selected_file_ids.indexOf( file_id );
//   if ( index > -1 ) {
//
//     MediaFilesSelector.selected_file_ids.splice( index, 1 );
//
//   }
//
// }
//
// MediaFilesSelector.AutoComplete = function ( field )  {
//
//   MediaFilesSelector.AutoComplete.data_table = 'media_files';
//   MediaFilesSelector.AutoComplete.data_column = 'path';
//   MediaFilesSelector.AutoComplete.hidden_name = 'media_files_path';
//
//   field.removeAttribute( 'onfocusin' );
//
//   field.removeAttribute( 'readonly' );
//   field.readOnly = false;
//
//   field.setAttribute( 'onkeyup', 'MediaFilesSelector.AutoComplete.search();' );
//
//   MediaFilesSelector.AutoComplete.field = field;
//
//   document.addEventListener( 'click', MediaFilesSelector.AutoComplete.onclick_func );
//
//   var files_table = document.getElementsByClassName( 'MediaFilesSelector_files_table' )[ 0 ];
//
// }
//
// MediaFilesSelector.AutoComplete.load = function  ()  {
//
//   var collection = document.getElementsByClassName( 'MediaFilesSelector' );
//   if ( ! collection.length )  {
//     return false;
//   }
//
//   var mfs = collection[ 0 ];
//   // console.log( mfs );
//   // console.log( mfs.parentElement );
//
//   var id_input = getElementByXpath( ".//input[@name='id']", mfs.parentElement );
//   // console.log( id_input );
//
//   var media_group_id = id_input.value;
//   // console.log( 'media_group_id: ' + media_group_id );
//
//   var table = mfs.getElementsByClassName( 'MediaFilesSelector_files_table' )[ 0 ];
//   var tbody = table.children[ 1 ];
//
//
//   /* fill selected_file_ids */
//   var file_rows = tbody.children;
//
//   var selected_file_ids = [];
//   for ( var c = 0; c < file_rows.length; c++ )  {
//
//     var row = file_rows [ c ];
//
//     var file_id = Number( row.getAttribute( 'data-file-id' ) );
//
//     selected_file_ids.push( file_id );
//
//   }
//   MediaFilesSelector.selected_file_ids = selected_file_ids;
//   /* ----- x ----- */
//
//
//   /* find out search URI */
//   var pathname = url.pathname;
//   var lastbar_pos = pathname.lastIndexOf( '/' );
//
//   var base_uri = pathname.substring( 0, lastbar_pos + 1 );
//
//   var search_uri = base_uri + 'media_files_media_groups/search?api_key=' + key;
//   var search_uri = search_uri + '&media_group_id=' + media_group_id;
//   /* ----- x ----- */
//
//
//   var handler = function( xhr ) {
//
//     if ( xhr.status == 200 )  {
//
//       var parser = new DOMParser();
//       var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );
//
//       var rows = xmldoc.getElementsByTagName( 'row' );
//
//       for ( var c = 0; c < rows.length; c++ )  {
//
//         var row = rows[ c ];
//
//         var media_file_id = row.children[ 0 ].textContent;
//         var media_file_path = row.children[ 1 ].textContent;
//
//         var new_tr = document.createElement( 'tr' );
//         new_tr.setAttribute( 'data-file-id', media_file_id );
//         tbody.appendChild( new_tr );
//
//         var new_path_td = document.createElement( 'td' );
//         new_path_td.textContent = media_file_path;
//         new_tr.appendChild( new_path_td );
//
//         var new_delete_td = document.createElement( 'td' );
//
//         var new_img = document.createElement( 'img' );
//         new_img.setAttribute( 'class', 'inline-icon' );
//         new_img.setAttribute( 'onclick', 'MediaFilesSelector.remove_file( this );' );
//         new_img.setAttribute( 'src', '/MediaShare/static/img/delete.png' );
//         new_delete_td.appendChild( new_img );
//
//         new_tr.appendChild( new_delete_td );
//
//       }
//
//     }
//
//   }
//
//   request( 'GET', search_uri, handler );
//
// }
//
// MediaFilesSelector.AutoComplete.search = function () {
//
//   var field = MediaFilesSelector.AutoComplete.field;
//
//   if ( ! field.value.length ) {
//
//     MediaFilesSelector.AutoComplete.delete_results_table();
//     return false;
//
//   }
//
//   // console.log( 'go on' );
//
//   var search_uri = MediaFilesSelector.AutoComplete.get_search_uri();
//
//   var uri = search_uri + encodeURIComponent( field.value );
//
//   // console.log( 'uri: ' + uri );
//
//   var handler = function ( xhr ) {
//
//     if ( xhr.status == 200 )  {
//
//       // console.log( xhr );
//
//       var data_table = MediaFilesSelector.AutoComplete.data_table;
//
//       var parser = new DOMParser();
//       var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );
//
//       var rows = xmldoc.getElementsByTagName( 'row' );
//
//       // console.log( rows );
//
//       var xpath = "//table[@class='MediaFilesSelector_AutoComplete_table']";
//       var already_created_table = getElementByXpath( xpath );
//
//       // console.log( already_created_table );
//
//       if ( already_created_table == null )  {
//
//         var new_table = document.createElement( 'table' );
//         new_table.setAttribute( 'class', 'MediaFilesSelector_AutoComplete_table' );
//
//         field.parentElement.appendChild( new_table );
//
//         var new_tbody = document.createElement( 'tbody' );
//         new_table.appendChild( new_tbody );
//
//         // console.log( new_table );
//
//         var table = new_table;
//         var tbody = new_tbody;
//
//       }
//       else {
//
//         var table = already_created_table;
//         // console.log( table );
//
//         var tbody = table.childNodes[ 0 ];
//
//         while ( tbody.firstChild )
//           tbody.removeChild( tbody.firstChild );
//
//       }
//
//       // console.log( table );
//
//       for ( var c = 0; c < rows.length; c++ )  {
//
//           var row = rows[ c ];
//           // console.log( row );
//
//           var id = xmldoc.evaluate(
//             ".//id",
//             row,
//             null,
//             XPathResult.FIRST_ORDERED_NODE_TYPE,
//             null
//           ).singleNodeValue;
//
//           var column_element = xmldoc.evaluate(
//             ".//" + MediaFilesSelector.AutoComplete.data_column,
//             row,
//             null,
//             XPathResult.FIRST_ORDERED_NODE_TYPE,
//             null
//           ).singleNodeValue;
//
//
//           var new_tr = document.createElement( 'tr' );
//           tbody.appendChild( new_tr );
//
//           var new_td = document.createElement( 'td' );
//           new_td.textContent = column_element.textContent;
//           new_td.setAttribute( 'data-id', id.textContent );
//           new_td.setAttribute( 'onclick', 'MediaFilesSelector.AutoComplete.select( this );' );
//           new_tr.appendChild( new_td );
//
//       }
//
//     }
//
//   }
//
//   request( 'GET', uri, handler );
//
// }
// MediaFilesSelector.AutoComplete.onclick_func = function ( event ) {
//
//   var field = MediaFilesSelector.AutoComplete.field;
//
//   if ( ! field.parentElement.contains ( event.target ) )  {
//
//     MediaFilesSelector.AutoComplete.end();
//
//   }
//
// }
// MediaFilesSelector.AutoComplete.delete_results_table = function () {
//
//   var collection = document.getElementsByClassName( 'MediaFilesSelector_AutoComplete_table' );
//
//   if ( ! collection.length )  {
//
//     return false;
//
//   }
//
//   var results_table = collection[ 0 ];
//
//   results_table.parentElement.removeChild( results_table );
//
// }
// MediaFilesSelector.AutoComplete.end = function () {
//
//   MediaFilesSelector.AutoComplete.delete_results_table();
//
//
//   var field = MediaFilesSelector.AutoComplete.field;
//
//   field.setAttribute( 'onfocusin', 'MediaFilesSelector.AutoComplete( this );' );
//
//   field.setAttribute( 'readonly', 'true' );
//   field.readOnly = true;
//
//   field.value = '';
//
//   // remove document onclick
//   document.removeEventListener( 'click', MediaFilesSelector.AutoComplete.onclick_func );
//
//
// }
// MediaFilesSelector.AutoComplete.get_search_uri = function () {
//
//   var pathname = url.pathname;
//   var lastbar_pos = pathname.lastIndexOf( '/' );
//   // console.log( lastbar_pos );
//
//   var base_uri = pathname.substring( 0, lastbar_pos + 1 );
//   var search_uri = base_uri + 'media_files/search?api_key=' + key;
//
//   var search_uri = search_uri + '&path=';
//
//   return search_uri;
//
// }
// MediaFilesSelector.AutoComplete.select = function ( td_element ) {
//
//   // console.log( td_element );
//
//   var files_table = document.getElementsByClassName( 'MediaFilesSelector_files_table' )[ 0 ];
//   // console.log( files_table );
//
//   var tbody = files_table.children[ 1 ];
//   // console.log( tbody );
//
//
//   var file_path = td_element.textContent;
//   // console.log( file_path );
//
//
//   var file_id = Number( td_element.getAttribute( 'data-id' ) );
//   // console.log( file_id );
//
//   var already_selected = ( MediaFilesSelector.selected_file_ids.indexOf( file_id ) > -1 );
//   if ( already_selected )  {
//
//     new Popup( 'Aviso', 'Arquivo com nome ' + file_path + ' já foi selecionado.' );
//     return false;
//
//   }
//   MediaFilesSelector.selected_file_ids.push( file_id );
//
//
//   var new_tr = document.createElement( 'tr' );
//   new_tr.setAttribute( 'data-file-id', file_id );
//   tbody.appendChild( new_tr );
//
//   var new_path_td = document.createElement( 'td' );
//   new_path_td.textContent = file_path;
//   new_tr.appendChild( new_path_td );
//
//   var new_delete_td = document.createElement( 'td' );
//
//   var new_img = document.createElement( 'img' );
//   new_img.setAttribute( 'class', 'inline-icon' );
//   new_img.setAttribute( 'onclick', 'MediaFilesSelector.remove_file( this );' );
//   new_img.setAttribute( 'src', '/MediaShare/static/img/delete.png' );
//   new_delete_td.appendChild( new_img );
//
//   new_tr.appendChild( new_delete_td );
//
//   MediaFilesSelector.AutoComplete.end();
//
// }
// MediaFilesSelector.fix_inputs = function () {
//
//   var media_files_selector_divs = document.getElementsByClassName( 'MediaFilesSelector' );
//   if ( ! media_files_selector_divs.length ) {
//
//     return false;
//
//   }
//
//   var div = media_files_selector_divs[ 0 ];
//
//   var results = document.evaluate(
//     "//table[@class='MediaFilesSelector_files_table']/tbody/tr",
//     div,
//     null,
//     XPathResult.ANY_TYPE,
//     null
//   );
//
//   var next = results.iterateNext();
//   var file_ids = [];
//
//   while ( next )  {
//
//     file_ids.push( next.getAttribute( 'data-file-id' ) );
//
//     var next = results.iterateNext();
//
//   }
//
//   for ( var c = 0; c < file_ids.length; c++ ) {
//
//     var file_id = file_ids[ c ];
//
//     var new_input = document.createElement( 'input' );
//     new_input.setAttribute( 'name', 'file[' + c + ']' );
//     new_input.setAttribute( 'type', 'hidden' );
//     new_input.setAttribute( 'value', file_id );
//
//     div.parentElement.appendChild( new_input );
//
//   }
//
// }
