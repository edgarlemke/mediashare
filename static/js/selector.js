Selector = function ( selector_div, entity_id ) {

  this.selector_div = selector_div;

  this.table = selector_div.getAttribute( 'data-table' );
  this.column = selector_div.getAttribute( 'data-column' );
  this.relationship_table = selector_div.getAttribute( 'data-relationship-table' );
  this.relationship_column = selector_div.getAttribute( 'data-relationship-column' );

  console.log( 'relationship_column: ' + this.relationship_column );


  var xpath = './table[@class="Selector_items_table"]';
  this.items_table = getElementByXpath( xpath, selector_div );

  var xpath = './div/input[@class="Selector_search"]';
  this.field = getElementByXpath( xpath, selector_div );


  this.onclick_func = (
    function ( object ) {
      return function ( event ) {

        if ( ! object.field.parentElement.contains ( event.target ) )  {

            Selector.end( object.table );

        }

      }
    }
  ) ( this );

  Selector.objects[ this.table ] = this;


  // Fill this.selected_ids
  var tbody = this.items_table.children[ 1 ];
  var item_rows = tbody.children;

  this.selected_ids = [];
  for ( var c = 0; c < item_rows.length; c++ )  {

    var row = item_rows[ c ];
    var item_id = Number( row.getAttribute( 'data-item-id' ) );
    this.selected_ids.push( item_id );

  }


  // Fill items table with already selected items

  if ( typeof entity_id != 'undefined' && entity_id != null )  {

    // Find out search URI
    var pathname = url.pathname;
    var lastbar_pos = pathname.lastIndexOf( '/' );

    var base_uri = pathname.substring( 0, lastbar_pos + 1 );

    console.log( 'base_uri: ' + base_uri );

    var search_uri = base_uri + this.relationship_table + '/search?api_key=' + key;
    var search_uri = search_uri + '&' + this.relationship_column + '=' + entity_id;
    // ----- x -----

    console.log( 'search_uri: ' + search_uri );

    var handler = ( function ( table ) {
      return function ( xhr ) {

        if ( xhr.status == 200 )  {

          var parser = new DOMParser();
          var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );

          var rows = xmldoc.getElementsByTagName( 'row' );

          for ( var c = 0; c < rows.length; c++ )  {

            var row = rows[ c ];

            var item_id = row.children[ 0 ].textContent;
            var column_value = row.children[ 1 ].textContent;

            var new_tr = document.createElement( 'tr' );
            new_tr.setAttribute( 'data-item-id', item_id );
            tbody.appendChild( new_tr );

            var new_path_td = document.createElement( 'td' );
            new_path_td.textContent = column_value;
            new_tr.appendChild( new_path_td );

            var new_delete_td = document.createElement( 'td' );

            var new_img = document.createElement( 'img' );
            new_img.setAttribute( 'class', 'inline-icon' );
            new_img.setAttribute( 'onclick', "Selector.remove_item( this, '" + table + "' );" );
            new_img.setAttribute( 'src', '/MediaShare/static/img/delete.png' );
            new_delete_td.appendChild( new_img );

            new_tr.appendChild( new_delete_td );

          }

        }

      }
    } ) ( this.table );

    request( 'GET', search_uri, handler );

  }

  // console.log( Selector.objects );

}


Selector.objects = {};


Selector.load = function () {

  var selectors = document.getElementsByClassName( 'Selector' );

  for( var c = 0; c < selectors.length; c++ )  {

    var selector_div = selectors[ c ];

    // Look into form for ID input to get entity id
    var id_input = getElementByXpath( ".//input[@name='id']", selector_div.parentElement );

    var entity_id = null;
    if ( id_input ) {
      var entity_id = id_input.value;
    }

    new Selector( selector_div, entity_id );

  }

}


Selector.enable_field = function ( field )  {

  var selector_div = field.parentElement.parentElement;
  var table = selector_div.getAttribute( 'data-table' );
  var object = Selector.objects[ table ];


  // Add click tracker event listener to document
  document.addEventListener( 'click', object.onclick_func );


  // Change field
  field.removeAttribute( 'onfocusin' );

  field.removeAttribute( 'readonly' );
  field.readOnly = false;

  field.setAttribute( 'onkeyup', "Selector.search( '" + object.table + "' );" );

}

Selector.disable_field = function ( field ) {



}


Selector.search = function ( table ) {

  // console.log( table );
  // console.log( Selector.objects );

  var object = Selector.objects[ table ];

  var field = object.field;


  if ( ! field.value.length ) {

    Selector.delete_results_table( table );
    return false;

  }


  var uri = Selector.search.get_uri( table, object.column, field.value );


  var handler = function ( xhr ) {

    if ( xhr.status == 200 )  {

      var parser = new DOMParser();
      var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );

      var rows = xmldoc.getElementsByTagName( 'row' );


      // Create or find table
      if ( typeof object.results_table == 'undefined' )  {

        console.log( field );

        var rect = field.getBoundingClientRect();
        console.log( rect );

        var new_wrapper = document.createElement( 'div' );
        new_wrapper.setAttribute( 'class', 'Selector_table_wrapper' );
        new_wrapper.setAttribute( 'style', 'top: ' + rect.bottom + 'px; left: ' + rect.x + 'px; width: ' + ( rect.right - rect.left ) + 'px;' );
        document.body.appendChild( new_wrapper );

        var new_table = document.createElement( 'table' );
        new_table.setAttribute( 'class', 'Selector_table' );
        new_wrapper.appendChild( new_table );

        var new_tbody = document.createElement( 'tbody' );
        new_table.appendChild( new_tbody );

        var results_table = new_table;
        var results_tbody = new_tbody;

        object.results_table = results_table;

      }
      else {

        var results_table = object.results_table;
        var results_tbody = results_table.childNodes[ 0 ];

        while ( results_tbody.firstChild )
          results_tbody.removeChild( results_tbody.firstChild );

      }


      for ( var c = 0; c < rows.length; c++ )  {

          var row = rows[ c ];

          var id = xmldoc.evaluate(
            ".//id",
            row,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null
          ).singleNodeValue;

          var column_element = xmldoc.evaluate(
            ".//" + object.column,
            row,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null
          ).singleNodeValue;


          var new_tr = document.createElement( 'tr' );
          results_tbody.appendChild( new_tr );

          var new_td = document.createElement( 'td' );
          new_td.textContent = column_element.textContent;
          new_td.setAttribute( 'data-id', id.textContent );
          new_td.setAttribute( 'onclick', "Selector.select( this, '" + table + "' );" );
          new_tr.appendChild( new_td );

      }

    }

  }

  request( 'GET', uri, handler );

}

Selector.search.get_uri = function ( table, column, value ) {

  var pathname = url.pathname;
  var lastbar_pos = pathname.lastIndexOf( '/' );
  // console.log( lastbar_pos );

  var base_uri = pathname.substring( 0, lastbar_pos + 1 );
  var search_uri = base_uri + table + '/search?api_key=' + key;

  var search_uri = search_uri + '&' + column + '=' + encodeURIComponent( value );

  return search_uri;

}


Selector.select = function ( td_element, table ) {

  console.log( table );

  var object = Selector.objects[ table ];


  var tbody = object.items_table.children[ 1 ];

  var column_value = td_element.textContent;


  var item_id = Number( td_element.getAttribute( 'data-id' ) );

  var already_selected = ( object.selected_ids.indexOf( item_id ) > -1 );
  if ( already_selected )  {

    new Popup( 'Aviso', 'Item ' + column_value + ' já foi selecionado!' );
    return false;

  }
  object.selected_ids.push( item_id );


  var new_tr = document.createElement( 'tr' );
  new_tr.setAttribute( 'data-item-id', item_id );
  tbody.appendChild( new_tr );

  var new_path_td = document.createElement( 'td' );
  new_path_td.textContent = column_value;
  new_tr.appendChild( new_path_td );

  var new_delete_td = document.createElement( 'td' );

  var new_img = document.createElement( 'img' );
  new_img.setAttribute( 'class', 'inline-icon' );
  new_img.setAttribute( 'onclick', "Selector.remove_item( this, '" + table + "' );" );
  new_img.setAttribute( 'src', '/MediaShare/static/img/delete.png' );
  new_delete_td.appendChild( new_img );

  new_tr.appendChild( new_delete_td );


  Selector.end( table );


}


Selector.remove_item = function ( remove_button, table ) {

  var object = Selector.objects[ table ];

  var tr = remove_button.parentElement.parentElement;
  var item_id = Number( tr.getAttribute( 'data-item-id', tr ) );

  tr.parentElement.removeChild( tr );

  var index = object.selected_ids.indexOf( item_id );
  if ( index > -1 ) {

    object.selected_ids.splice( index, 1 );

  }

}


Selector.end = function ( table ) {

  var object = Selector.objects[ table ];


  Selector.delete_results_table( table );


  // Remove document click tracker
  document.removeEventListener( 'click', object.onclick_func );


  // Change field
  var field = object.field;

  field.setAttribute( 'onfocusin', 'Selector.enable_field( this );' );

  field.setAttribute( 'readonly', 'true' );
  field.readOnly = true;

  field.value = '';

  // delete Selector.objects[ table ];

}


Selector.delete_results_table = function ( table ) {

  var object = Selector.objects[ table ];

  if ( typeof object.results_table == 'undefined' ) {
    return false;
  }

  // Delete table wrapper
  var results_table = object.results_table;
  console.log( results_table );

  var wrapper = results_table.parentElement;
  wrapper.parentElement.removeChild( wrapper );

  // Delete results table from object
  delete object.results_table;

}


Selector.fix_inputs = function  ()  {

  for ( var table in Selector.objects )  {

    var object = Selector.objects[ table ];
    // console.log( object );

    var results = document.evaluate(
      './table[@class="Selector_items_table"]/tbody/tr',
      object.selector_div,
      null,
      XPathResult.ANY_TYPE,
      null
    );
    console.log( results );

    var next = results.iterateNext();
    var item_ids = [];
    while( next ) {

      console.log( next );

      item_ids.push( next.getAttribute( 'data-item-id' ) );
      var next = results.iterateNext();

    }
    console.log( item_ids );

    for( var c = 0; c < item_ids.length; c++ )  {

      var item_id = item_ids[ c ];

      var new_input = document.createElement( 'input' );
      new_input.setAttribute( 'name', 'Selector_items[' + c + ']' );
      new_input.setAttribute( 'type', 'hidden' );
      new_input.setAttribute( 'value', item_id );

      object.field.parentElement.parentElement.appendChild( new_input );

      console.log( new_input );

    }

  }

}
