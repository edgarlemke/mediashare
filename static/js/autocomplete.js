AutoComplete = function ( field ) {

  var data_table = field.getAttribute( 'data-table' );
  var data_column = field.getAttribute( 'data-column' );
  var hidden_name = field.getAttribute( 'data-hidden-name' );

  AutoComplete.objects[ data_table ] = {
    'field'         : field ,
    'data_column'   : data_column ,
    'hidden_name'   : hidden_name ,
    'onclick_func'  : null ,
    'results_table' : null
  }


  field.removeAttribute( 'onfocusin' );

  field.removeAttribute( 'readonly' );
  field.readOnly = false;

  field.setAttribute( 'onkeyup', 'AutoComplete.search( this );' );

  var obj = AutoComplete.objects[ data_table ];
  obj.onclick_func = (
    function ( data_table ) {
      return function ( event ) {

        if ( ! field.parentElement.contains ( event.target ) )  {

            AutoComplete.end( data_table );

        }

      }
    }
  ) ( data_table );

  document.addEventListener( 'click', obj.onclick_func );

}

AutoComplete.objects = {}

AutoComplete.end = function ( data_table )  {

  if ( ! ( data_table in AutoComplete.objects /*data_table in Object.keys( AutoComplete.objects*/ ) ) {
    console.log( 'No data_table ' + data_table + ' in ' + Object.keys( AutoComplete.objects ) );
    return false;
  }

  var obj = AutoComplete.objects[ data_table ];

  AutoComplete.delete_results_table( obj );

  obj.field.setAttribute( 'onfocusin', "new AutoComplete( this );" );
  obj.field.setAttribute( 'readonly', 'true' );
  obj.field.readOnly = true;

  // remove field content if it matches no result
  var hidden_input = AutoComplete.find_hidden_input( obj );
  if ( ! hidden_input ) {

    obj.field.value = '';
    // hidden_input.parentElement.removeChild( hidden_input );

  }

  // remove document onclick
  document.removeEventListener( 'click', obj.onclick_func );

  delete AutoComplete.objects[ data_table ];

}

AutoComplete.delete_results_table = function ( obj ) {

  // console.log( obj );

  if ( obj.results_table !== null ) {

    obj.results_table.parentElement.removeChild( obj.results_table );
    obj.results_table = null;

  }

}

AutoComplete.search = function ( field ) {

    var data_table = field.getAttribute( 'data-table' );

    var obj = AutoComplete.objects[ data_table ];

    if ( ! field.value.length ) {

      AutoComplete.delete_results_table( obj );
      return false;

    }
    else {

      var hidden_input = AutoComplete.find_hidden_input( obj );
      if ( hidden_input ) {

        hidden_input.parentElement.removeChild( hidden_input );

      }

    }

    var search_uri = AutoComplete.get_search_uri( data_table, obj.data_column );

    var uri = search_uri + encodeURIComponent( field.value );

    var handler = ( function ( obj ) {
        return function ( xhr ) {
            if ( xhr.status == 200 )  {

                var data_table = Object.entries( AutoComplete.objects ).find( i => i[ 1 ] === obj )[ 0 ];

                var parser = new DOMParser();
                var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );

                var rows = xmldoc.getElementsByTagName( 'row' );
                // console.log( rows );

                var xpath = "//table[@class='AutoComplete_table' and @data-table='" + data_table + "']";
                var already_created_table = getElementByXpath( xpath );

                if ( already_created_table == null )  {

                  var rect = field.getBoundingClientRect();
                  console.log( rect );

                  var new_table = document.createElement( 'table' );
                  new_table.setAttribute( 'class', 'AutoComplete_table' );
                  new_table.setAttribute( 'style', 'width: ' + ( rect.right - rect.left ) + 'px;' );
                  new_table.setAttribute( 'data-table', data_table );

                  obj.field.parentElement.appendChild( new_table );

                  var new_tbody = document.createElement( 'tbody' );
                  new_table.appendChild( new_tbody );

                  var table = new_table;
                  var tbody = new_tbody;

                }
                else  {

                  var table = already_created_table;
                  // console.log( table );

                  var tbody = table.childNodes[ 0 ];

                  while ( tbody.firstChild )
                    tbody.removeChild( tbody.firstChild );

                }
                obj.results_table = table;
                // console.log( obj.results_table );

                for ( var c = 0; c < rows.length; c++ )  {

                    var row = rows[ c ];
                    // console.log( row );

                    var id = xmldoc.evaluate( ".//id", row, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null  ).singleNodeValue;
                    var column_element = xmldoc.evaluate( ".//" + obj.data_column, row, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;


                    var new_tr = document.createElement( 'tr' );
                    tbody.appendChild( new_tr );

                    var new_td = document.createElement( 'td' );
                    new_td.textContent = column_element.textContent;
                    new_td.setAttribute( 'data-id', id.textContent );
                    new_td.setAttribute( 'onclick', 'AutoComplete.fill( \'' + data_table + '\', this );' );
                    new_tr.appendChild( new_td );

                }

            }
            else {

                console.log( xhr );

            }
        }
    } ) ( obj );

    request( 'GET', uri, handler );

}

AutoComplete.get_search_uri = function ( entity, column ) {

  var pathname = url.pathname;
  var lastbar_pos = pathname.lastIndexOf( '/' );
  // console.log( lastbar_pos );

  var base_uri = pathname.substring( 0, lastbar_pos + 1 );
  var search_uri = base_uri + entity + '/search?api_key=' + key;

  var search_uri = search_uri + '&' + column + '=';

  return search_uri;

}

AutoComplete.fill = function ( data_table, td_element )    {

    var obj = AutoComplete.objects[ data_table ];


    var id = td_element.getAttribute( 'data-id' );
    obj.field.value = td_element.textContent;
    obj.field.readonly = true;

    var hidden_input = AutoComplete.find_hidden_input( obj );
    if ( ! hidden_input )  {

      var hidden_input = document.createElement( 'input' );

      hidden_input.setAttribute( 'type', 'hidden' );
      hidden_input.setAttribute( 'name', obj.hidden_name );
      obj.field.parentElement.appendChild( hidden_input );

    }

    hidden_input.setAttribute( 'value', id );
    obj.hidden_input = hidden_input;

    AutoComplete.end( data_table );

}

AutoComplete.find_hidden_input = function ( obj ) {

  var xpath = 'input[@type="hidden" and @name="' + obj.hidden_name + '"]';
  return getElementByXpath( xpath, obj.field.parentElement );

}

AutoComplete.load = function () {

  var autocomplete_elements = document.getElementsByClassName( 'AutoComplete' );
  var xpath = 'input[@type="hidden" and contains( @name, "id")]';

  for ( var c = 0; c < autocomplete_elements.length; c++ )  {

    var element = autocomplete_elements[ c ];
    var parent = element.parentElement;

    var data_table = element.getAttribute( 'data-table' );
    var data_column = element.getAttribute( 'data-column' );

    var hidden_input = getElementByXpath( xpath, parent );

    if ( ! hidden_input.value )  {
      continue;
    }


    var search_uri = AutoComplete.get_search_uri( data_table, 'id' );
    var uri = search_uri + hidden_input.value;

    var handler = (
      function ( element, column_name )  {
        return function ( xhr )  {

          if ( xhr.status == 200 )  {

            var parser = new DOMParser();
            var xmldoc = parser.parseFromString( xhr.responseText, 'text/xml' );

            var xpath = '//result/row/' + column_name;

            var column_value = xmldoc.evaluate( xpath, xmldoc, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.textContent;

            element.value = column_value;

          }

        }
      }
    ) ( element, data_column );

    request( 'GET', uri, handler );

  }

}
