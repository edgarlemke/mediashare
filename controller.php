<?php

/*

ARQUIVO controller.php

É responsável pelo gerenciamento das requisições REST junto com o Model
( arquivo model.php ) e o View. Também guarda funções de autenticação.

*/

ini_set( 'upload_max_size' , '256M' );
ini_set( 'post_max_size' , '256M' );


include_once( 'config.php' );
include_once( 'model.php' );
include_once( 'view.php' );

include_once( 'ldap.php' );

// include_once( 'controller/units.php' );
include_once( 'controller/players.php' );
include_once( 'controller/auth.php' );
include_once( 'controller/schedulings.php' );
include_once( 'controller/players_reports.php' );


// URI para home page privada
$home = 'home';


// Trata requisição
function handle_request ()  {

  // Incluso a partir de view.php
  global $template;
  global $private_template;

  global $home;

  if ( isset( $_GET[ 'api_key' ] ) )
    $api_key = $_GET[ 'api_key' ];
  else
    $api_key = null;

  if ( isset( $_GET[ 'client_key' ] ) )
    $client_key = $_GET[ 'client_key' ];
  else
    $client_key = null;


  // Extrair método HTTP da requisição
  $method = $_SERVER[ 'REQUEST_METHOD' ];

  // Fatiar URI para identificar recurso
  $URI =  $_SERVER[ 'REQUEST_URI' ];
  $base_URI = dirname( $_SERVER[ 'SCRIPT_NAME' ] );
  $stripped_URI = substr( $URI, strlen( $base_URI ) );

  $resources = read( 'resources', array( '*' ) );

  if ( ! $resources )
    throw new Exception( 'Não foi possível obter dados de recursos REST da base de dados' ); // TODO: HTTP 500


  // Limpa parâmetros do URI para identificar recurso
  $interrogation_pos = strpos( $stripped_URI, '?' );

  if ( $interrogation_pos !== false ) {
    $URI_withouth_parameters = substr( $stripped_URI, 0, $interrogation_pos );
  }
  else {
    $URI_withouth_parameters = $stripped_URI;
  }


  // Recursos comuns
  foreach ( $resources as $resource )  {

    if ( $resource[ 'URI' ] == $URI_withouth_parameters )
      break;

    $resource = null;
  }

  // trigger_error( 'resource: ' . print_r( $resource, true ) );

  // TODO: recursos para rotinas

  // Recurso para login
  if ( $URI_withouth_parameters == '/login' && $method == 'POST' ) {

    $username = $_POST[ 'username' ];
    $password = $_POST[ 'password' ];

    // if ( ! check_password( $username, $password ) ) {
    if ( ! authenticate( $username, $password ) ) {

      // TODO: mensagem de erro na tela de login, senha incorreta
      header( "Location: $public_root", 302 );
      exit();

    }

    $users = read(
      'users',
      array( 'id', 'permission' ),
      $conditions= new Condition( 'username', '==', $username )
    );

    $user_id = $users[ 0 ][ 'id' ];
    $user_permission = $users[ 0 ][ 'permission' ];

    if ( $user_permission == 0 )  {

      // TODO: mensagem de erro na tela de login, usuário desativado
      header( "Location: $public_root", 403 );
      exit();

    }

    // Se usuário e senha estão corretos, apaga chave de API atual
    delete_api_credential( $api_key );

    $api_key = create_api_credential( $user_id )[ 'key' ];

    header( "Location: $public_roothome?api_key=$api_key", 302 );
    exit();

  }
  // else if ( $URI_withouth_parameters == '/client_login' && $method == 'POST' )  {
  //
  //   $client_key = $_POST[ 'client_key' ];
  //
  //   if ( ! check_client_key( $cliente_key ) ) {
  //     exit();
  //   }
  //
  //   header( 200 );
  //   print( 'OK' );
  //
  //   exit();
  //
  // }
  else if ( $URI_withouth_parameters == '/logout' && $method == 'GET' ) {

    delete_api_credential( $api_key );

    header( "Location: $public_root", 302 );
    exit();

  }

  // Recurso para busca
  if ( strpos( $URI_withouth_parameters, 'search' ) !== false )  {

    // Encontra recurso a ser buscado
    $pieces = explode( '/', $URI_withouth_parameters );

    $search_pos = array_search( 'search', $pieces );

    $entity = $pieces[ $search_pos - 1 ];

    $conditions = null;
    foreach( $_GET as $key => $value )  {

      if ( $key == 'api_key' || ( $entity != 'players' && $key == 'client_key' ) )
        continue;

      $like_sql = '%'. str_replace( ' ', '%', $value ) . '%';
      $new_like_condition = new Condition( $key, 'LIKE', $like_sql );

      if ( $conditions == null )  {

        $conditions = $new_like_condition;

      }
      else {

        $conditions = new Condition( $conditions, 'AND', $new_like_condition );

      }

    }

    if ( $entity == 'users' )  {

      $columns = array( '`users`.`id`', '`users`.`name`', '`users`.`username`', '`units`.`name` AS `units_name`', '`users`.`permission`' );
      $joins = array( new LeftJoin( 'users', 'unit_id', 'units', 'id' ) );

    }
    elseif ( $entity == 'schedulings' ) {

      if ( $client_key == null )  {
        $columns = array( '`schedulings`.`id`', '`schedulings`.`start`', '`schedulings`.`end`', '`units`.`name` AS `units_name`' );
      }
      else {
        $columns = array( '`schedulings`.`id`', '`schedulings`.`start`', '`schedulings`.`end`', '`schedulings`.`unit_id` AS `unit_id`' );
      }

      $joins = array( new LeftJoin( 'schedulings', 'unit_id', 'units', 'id' ) );

    }
    elseif ( $entity == 'media_files' ) {

      $columns = array( '`media_files`.`id`', '`media_files`.`path`', '`users`.`username` AS `user_username`', '`media_files`.`timestamp`' );
      $joins = array( new LeftJoin( 'media_files', 'user_id', 'users', 'id' ) );

    }
    elseif( $entity == 'media_files_media_groups' ) {

      $columns = array( '`media_files`.`id`', '`media_files`.`path`' );
      $joins = array( new LeftJoin( 'media_files_media_groups', 'media_file_id', 'media_files', 'id' ) );

    }
    // elseif( $entity == 'media_groups_units' ) {
    //
    //   $columns = array( '`media_groups`.`id`', '`media_groups`.`name`' );
    //   $joins = array( new InnerJoin( 'media_groups_units', 'media_group_id', 'media_groups', 'id' ) );
    //
    // }
    elseif( $entity == 'media_groups_schedulings' ) {

      $columns = array( '`media_groups`.`id`, `media_groups`.`name`' );
      $joins = array( new LeftJoin( 'media_groups_schedulings', 'media_group_id', 'media_groups', 'id' ) );

    }

    elseif( $entity == 'players' ) {

      if ( $client_key == null )  {
        $columns = array(
          '`players`.`id`',
          '`units`.`name` AS unit_name',
          '`players_reports`.`ip`',
          'MAX( `players_reports`.`timestamp` ) AS last_report',
          '`players`.`inclusion`',
          '`players`.`hostname`',
          '`players`.`hardware`',
          '`players`.`serial`',
          '`players`.`client_version`'
        );

        $joins = array(
          new InnerJoin( 'players', 'id', 'players_reports', 'player_id' ) ,
          new InnerJoin( 'players', 'unit_id', 'units', 'id' )
        );
      }
      else {

        $columns = array( 'id' );
        $joins = null;

      }

    }

    else {

      $columns = array( '*' );
      $joins = null;

    }

    $results = read (
        $entity,
        $columns,
        $conditions,
        $sorting= null,
        $splitting= null,
        $joins = $joins
    );


    // if( $entity == 'schedulings' && $client_key !== null )  {
    //
    //   trigger_error( 'results: ' . print_r( $results, true ) );
    //
    //   $has_scheduling_now = false;
    //   $now = time();
    //
    //   $next_start = null;
    //
    //   foreach( $results as $row ) {
    //
    //     $start = strtotime( $row[ 'start' ] );
    //     $end = strtotime( $row[ 'end' ] );
    //
    //     if ( $now >= $start && $now < $end )  {
    //       $has_scheduling_now = true;
    //       break;
    //     }
    //
    //     if ( $next_start == null )  {
    //       $next_start = $start;
    //     }
    //     elseif ( $start < $next_start ) {
    //       $next_start = $start;
    //     }
    //
    //   }
    //
    //   if ( ! $has_scheduling_now )  {
    //     // forge fake scheduling
    //
    //     if ( $next_start == null )  {
    //         $end = $now + ( 60 * 60 * 24 );
    //     }
    //     else {
    //         $end = $next_start;
    //     }
    //
    //     $new_row = array(
    //       'start' => date( 'Y-m-d H:i:s', $now ),
    //       'end' => date( 'Y-m-d H:i:s', $end ),
    //       'id' => null,
    //       'unit_id' => 1
    //     );
    //
    //     trigger_error( '$new_row: ' . print_r( $new_row, true ) );
    //
    //     array_push( $results, $new_row );
    //
    //   }
    //
    // }
    //
    // trigger_error( 'results: ' . print_r( $results, true ) );


    $dom = new DOMDocument( '1.0', 'utf-8' );

    $result = $dom->createElement( 'result' );
    $dom->appendChild( $result );

    foreach( $results as $row ) {

      $new_row = $dom->createElement( 'row' );
      $result->appendChild( $new_row );

      foreach( $row as $column_name => $column_value )  {

        $name_element = $dom->createElement( $column_name );
        $new_row->appendChild( $name_element );

        $value_element = $dom->createTextNode( $column_value );
        $name_element->appendChild( $value_element );

      }

    }

    print( $dom->saveXML() );
    exit();

  }



  if ( $resource == null ) {

    http_response_code( 302 );
    header( "Location: $public_root" );

    throw new Exception( "Não existe recurso que corresponde à URI solicitada: $URI_withouth_parameters" );
    exit();

  }


  $conditions = new Condition( 'resource_id', '==', $resource[ 'id' ] );
  $valid_methods_rows = read( 'resources_valid_methods', array( 'method' ), $conditions= $conditions );
  $valid_methods = array();

  $method_ok = false;
  foreach ( $valid_methods_rows as $row ) {

    if ( $method == $row[ 'method' ] )  {
      $method_ok = true;
      break;
    }

  }

  if ( ! $method_ok )
    throw new Exception ( "Recurso com ID ${resource[ 'id' ]} não suporta método $method" );


  // Se usuário manda chave da API via GET, já tem uma chave
  if ( ! is_null( $api_key ) )  {

    global $credential;

    trigger_error( $api_key );

    $credential = load_api_credential( $api_key );
    $logged_in = ! is_null( $credential[ 'user_id' ] );

  }
  else if ( ! is_null( $client_key ) )  {

    $logged_in = true;

  }
  // Se não, cria uma nova chave sem usuário associado
  else {

    global $credential;

    $credential = create_api_credential();
    $api_key = $credential[ 'key' ];
    $logged_in = false;

  }

  //print( 'logged_in: '. print_r( $logged_in, true ) );

  if ( ( ! $logged_in ) && $resource[ 'private' ] ) {

    header( "Location: $public_root", 302 );
    exit();

  }

  switch( $method ) {

    case 'GET':
      $body = GET( $resource, $logged_in );
      break;

    case 'POST':
      $body = POST( $resource, $logged_in );
      break;

    // case 'PUT':
    //   break;

    case 'PATCH':
      $body = PATCH( $resource, $logged_in );
      break;

    case 'DELETE':
      $body = resDELETE( $resource, $logged_in );
      break;

    // case 'HEAD':
    //   break;

    // case 'OPTION':
    //   break;

  }

  // print( '<!-- '. $body . '-->' );

  $body = str_replace( '$api_key', $api_key, $body );
  print( $body );


  return true;

}


function GET ( $resource, $logged_in ) {

  // print( "<!--" . print_r( $resource, true ) . "-->" );

  $is_users_entity = ( $resource[ 'entity_name' ] == 'users' );
  $is_schedulings_entity = ( $resource[ 'entity_name' ] == 'schedulings' );
  $is_players_config_entity = ( $resource[ 'entity_name' ] == 'players_config' );


  $is_players_entity = ( $resource[ 'entity_name' ] == 'players' );
  if ( $is_players_entity && ( strpos( $resource[ 'URI' ], 'screenshot' ) !== false ) ) {

    return Players::GET_screenshot( $resource, $logged_in );

  }



  $view_id = $resource[ 'view_id' ];
  $cond = new Condition( 'id', '==', $view_id );
  $views = read( 'views', array( '*' ), $conditions= $cond );

  if ( ! $views )
    throw new Exception ( "Não foi possível carregar view com id {$view_id}" );

  $content = $views[ 0 ][ 'html' ];
  // print( "<!-- resource content:  $content -->" );


  // Se conter edit na URI, renderizar como um formulário de atualização
  // incluindo as informações do recurso nos campos
  if ( strpos( $resource[ 'URI' ], 'edit' ) !== false )  {


    if ( $is_users_entity ) {
      $columns = array( 'id', 'name', 'username', 'unit_id', 'permission' );
    }
    else if ( $is_players_config_entity ) {
      $columns = array( 'lease_time' );
    }
    else {
      $columns = array( '*' );
    }

    $results = read (
      $resource[ 'entity_name' ],
      $columns,
      $conditions= new Condition( 'id', '==', $resource[ 'entity_id' ] )
    );

    if ( $is_schedulings_entity ) {

      $start = $results[ 0 ][ 'start' ];
      $end = $results[ 0 ][ 'end' ];

      $start_date_obj = new DateTime( $start );
      $end_date_obj = new DateTime( $end );

      $start_date = $start_date_obj->format( "Y-m-d" );
      $start_time = $start_date_obj->format( "H:i" );

      $end_date = $end_date_obj->format( "Y-m-d" );
      $end_time = $end_date_obj->format( "H:i" );

      $field_fillers = $results[ 0 ];
      unset( $field_fillers[ 'start' ] );
      unset( $field_fillers[ 'end' ] );

      $field_fillers[ 'start_date' ] = $start_date;
      $field_fillers[ 'start_time' ] = $start_time;
      $field_fillers[ 'end_date' ] = $end_date;
      $field_fillers[ 'end_time' ] = $end_time;

    }
    else {

      $field_fillers = $results[ 0 ];

    }

  }
  else {

    $field_fillers = null;

  }

  // trigger_error( 'field_fillers: '. print_r( $field_fillers, true ) );

  // $content = staticfy_html( $content, $field_fillers );
  // print( "<!-- staticfied content:  $content -->" );

  $should_merge_views = $resource[ 'merge_view' ];

  if ( $should_merge_views ) {

    $final_view = merge_views( $content, $logged_in );

  }
  else {

    $final_view = $content;

  }


  $final_view = staticfy_html( $final_view, $field_fillers );


  return $final_view;

}

function POST ( $resource, $logged_in ) {


  $is_media_files_entity = ( $resource[ 'entity_name' ] == 'media_files' );
  if ( $is_media_files_entity )  {

      return POST_file( $resource, $logged_in );

  }
  $is_media_groups_entity = ( $resource[ 'entity_name' ] == 'media_groups' );
  if ( $is_media_groups_entity )  {

      return POST_media_group( $resource, $logged_in );

  }
  $is_players_entity = ( $resource[ 'entity_name' ] == 'players' );
  if ( $is_players_entity ) {

    return Players::POST( $resource, $logged_in );

  }
  $is_schedulings_entity = ( $resource[ 'entity_name' ] == 'schedulings' );
  if ( $is_schedulings_entity ) {

    return Schedulings::POST( $resource, $logged_in );

  }
  $is_players_reports_entity = ( $resource[ 'entity_name' ] == 'players_reports' );
  if ( $is_schedulings_entity ) {

    return PlayersReports::POST( $resource, $logged_in );

  }


  $mangled_POST = $_POST;

  $is_users_entity = ( $resource[ 'entity_name' ] == 'users' );
  if ( $is_users_entity )  {

    unset( $mangled_POST[ 'password' ] );
    $mangled_POST[ 'password_hash' ] = protect_password( $_POST[ 'password' ] );

  }


  // Create new entity from POST data
  $created = create( $resource[ 'entity_name' ], $mangled_POST );


  if ( $is_users_entity )  {

    $columns = array( 'id', 'username', 'name', 'unit_id', 'permission' );

  }
  else {

    $columns = array( '*' );

  }


  $rows = read (
      $resource[ 'entity_name' ],
      $columns,
      $conditions= null,
      $sorting= array( 'id' => 'DESC' ),
      $splitting= [ 0, 1 ]
  );

  $new_entity = $rows[ 0 ];
  $new_entity_id = $new_entity[ 'id' ];


  // Create new REST resource
  $default_valid_methods = array( 'GET', 'DELETE', 'PATCH' );

  // Create new REST resource
  $uri = "${resource[ 'URI' ]}/${new_entity_id}";

  $new_resource_id = create_REST_resource(
    $uri,
    $resource[ 'entity_name' ],
    $new_entity_id,
    null,
    $logged_in,
    $default_valid_methods
  );


  $results = read (
      'views',
      array( 'id' ),
      $conditions = new Condition (
        'edit_view_class', '==', $resource[ 'entity_name' ]
      )
  );
  $edit_view = $results[ 0 ];


  $uri = "${resource[ 'URI' ]}/${new_entity_id}/edit";

  $new_resource_edit_id = create_REST_resource(
    $uri,
    $resource[ 'entity_name' ],
    $new_entity_id,
    $edit_view[ 'id' ],
    $logged_in,
    array( 'GET' )
  );


  return forge_entity_table( $new_entity, $logged_in );

}

function POST_file ( $resource, $logged_in ) {

 // trigger_error( print_r( $_FILES, true ) );
 $target_dir = 'uploads/';

 foreach ( $_FILES as $file ) {

   trigger_error( print_r( $file, true ) );

   $target_file = $target_dir . basename( $file[ 'name' ] );

   trigger_error( print_r( $target_file, true ) );

   if( file_exists( $target_file ) ) {

     http_response_code( 409 );
     print( "There's already a file with the name of the uploaded file: ${file['name']}" );
     exit();

   }

   if ( $file[ 'size' ] > 250 * 1024 * 1024 )  {

     http_response_code( 413 );
     print( "File bigger than 250 MB." );
     exit();

   }

   $moved = move_uploaded_file( $file[ 'tmp_name' ], $target_file );
   if ( ! $moved ) {

     http_response_code( 500 );
     print( "Couldn't move uploaded file." );
     exit();

   }


   $hash = shell_exec( "sha256sum -b \"$target_file\" | cut -d ' ' -f 1" );

   $results = read(
     'media_files',
     array('*'),
     $conditions= new Condition( 'hash', '==', $hash )
   );

   if ( count( $results ) )  {

     throw new Exception ( "Duplicated file." );

   }

   global $credential;

   $data = array(
     'user_id' =>  $credential[ 'user_id' ] ,
     'path' => $file[ 'name' ] ,
     'hash' => $hash
   );

   create( 'media_files', $data );

   $results = read(
     'media_files',
     array( '*' ),
     $conditions= null,
     $sorting= array( 'id' => 'DESC' ),
     $splitting= array( 0, 1 )
   );
   $new_entity = $results[ 0 ];
   $new_entity_id = $new_entity[ 'id' ];

   trigger_error( 'new_entity_id: '. $new_entity_id );

   $uri = "${resource[ 'URI' ]}/${new_entity_id}";
   $new_resource_id = create_REST_resource(
     $uri,
     'media_files',
     $new_entity_id,
     null,
     $logged_in,
     array( 'GET', 'DELETE' )
   );

   trigger_error( 'new_resource_id: '. $new_resource_id );

  }

  return '';

}

function POST_media_group ( $resource, $logged_in ) {

  $name = $_POST[ 'name' ];
  $Selector_items = $_POST[ 'Selector_items' ];

  $data = array(
    'name' => $name
  );
  create( 'media_groups', $data );

  $results = read(
    'media_groups',
    array( '*' ),
    $conditions= null,
    $sorting= array( 'id' => 'DESC' ),
    $splitting= array( 0, 1 )
  );

  $new_entity = $results[ 0 ];
  $new_entity_id = $new_entity[ 'id' ];

  trigger_error( 'new_entity_id: ' . $new_entity_id );

  $uri = "${resource[ 'URI' ]}/${new_entity_id}";
  $new_resource_id = create_REST_resource(
    $uri,
    'media_groups',
    $new_entity_id,
    null,
    $logged_in,
    array( 'GET', 'DELETE', 'PATCH' )
  );

  trigger_error( 'new_resource_id: ' . $new_resource_id );


  $results = read (
      'views',
      array( 'id' ),
      $conditions = new Condition (
        'edit_view_class', '==', $resource[ 'entity_name' ]
      )
  );
  $edit_view = $results[ 0 ];

  // trigger_error( 'edit_view: ' . print_r( $edit_view, true) );

  $uri = "${resource[ 'URI' ]}/${new_entity_id}/edit";

  $new_resource_edit_id = create_REST_resource(
    $uri,
    $resource[ 'entity_name' ],
    $new_entity_id,
    $edit_view[ 'id' ],
    $logged_in,
    array( 'GET' )
  );


  foreach ( $Selector_items as $key => $file_id )  {

    $data = array(
      'media_group_id' => $new_entity_id,
      'media_file_id' => $file_id
    );

    create( 'media_files_media_groups', $data );

  }

  return '';

}



function create_REST_resource ( $uri, $entity_name, $entity_id, $view_id, $private, $valid_methods ) {

  $data = array(  'URI'         => $uri ,
                  'entity_name' => $entity_name ,
                  'entity_id'   => $entity_id ,
                  'view_id'     => $view_id ,
                  'private'     => $private ,
                  'merge_view'  => 0   );

  $created_resource = create( 'resources', $data );

  $rows = read( 'resources', [ 'id' ], $conditions= null, $sorting= array( 'id' => 'DESC' ), $splitting= [ 0, 1 ] );
  $new_resource_id = $rows[ 0 ][ 'id' ];


  // Create resource valid methods
  foreach( $valid_methods as $each_method ) {

      $data = array(
          'method' => $each_method,
          'resource_id' => $new_resource_id
      );

      $created_resource_valid_method = create(
          'resources_valid_methods',
          $data
      );

  }


  return $new_resource_id;

}

function resDELETE ( $resource, $logged_in ) {

  $is_media_files_entity = ( $resource[ 'entity_name' ] == 'media_files' );
  if ( $is_media_files_entity )  {
      DELETE_file( $resource, $logged_in );
  }

  $is_media_groups_entity = ( $resource[ 'entity_name' ] == 'media_groups' );
  if ( $is_media_groups_entity )  {
      DELETE_media_group ( $resource, $logged_in );
  }

  $is_schedulings_entity = ( $resource[ 'entity_name' ] == 'schedulings' );
  if ( $is_schedulings_entity ) {

      Schedulings::DELETE( $resource, $logged_in );

  }

  // Apaga linha da tabela do recurso
  $condition = new Condition( 'id', '==', $resource[ 'entity_id' ] );
  delete( $resource[ 'entity_name' ], $conditions= $condition );

  // Apaga métodos válidos para URI da tabela resources_valid_methods
  $condition = new Condition( 'resource_id', '==', $resource[ 'id' ] );
  delete( 'resources_valid_methods', $conditions= $condition );

  // Apaga URI da tabela resources
  $condition = new Condition( 'id', '==', $resource[ 'id' ] );
  delete( 'resources', $conditions= $condition );

  http_response_code( 204 );
  exit();

}


function DELETE_file( $resource, $logged_in ) {

  $media_file_id = $resource[ 'entity_id' ];

  $results = read( 'media_files', [ 'path' ], $conditions= new Condition( 'id', '==', $media_file_id ) );

  $path = $results[ 0 ][ 'path' ];

  shell_exec( "rm -f uploads/$path" );

  //
  $condition = new Condition( 'media_file_id', '==', $media_file_id );
  delete( 'media_files_media_groups', $conditions= $condition );


}

function DELETE_media_group ( $resource, $logged_in ) {

  $media_group_id = $resource[ 'entity_id' ];

  delete(
    'media_files_media_groups',
    new Condition( 'media_group_id', '==', $media_group_id )
  );

  // Apaga linha da tabela do recurso
  $condition = new Condition( 'id', '==', $media_group_id );
  delete( 'media_groups', $conditions= $condition );

  // Apaga métodos válidos para URI da tabela resources_valid_methods
  $condition = new Condition( 'resource_id', '==', $resource[ 'id' ] );
  delete( 'resources_valid_methods', $conditions= $condition );

  // Apaga URI da tabela resources
  $condition = new Condition( 'id', '==', $resource[ 'id' ] );
  delete( 'resources', $conditions= $condition );

  http_response_code( 204 );
  exit();


}


function PATCH ( $resource, $logged_in )    {


    ini_set( "allow_url_fopen", true );

    $data = file_get_contents( "php://input", "r" );
    trigger_error( 'data: ' . print_r( $data, true ) );

    $parsed = array();
    parse_str( $data, $parsed );

    // trigger_error( print_r( $parsed, true ) );

    $is_media_groups_entity = ( $resource[ 'entity_name' ] == 'media_groups' );
    if ( $is_media_groups_entity )  {

      return PATCH_media_group( $resource, $logged_in, $parsed );

    }

/*    $is_units_entity = ( $resource[ 'entity_name' ] == 'units' );
    if ( $is_units_entity )  {

      return Units::PATCH( $resource, $logged_in, $parsed );

    } */
    $is_schedulings_entity = ( $resource[ 'entity_name' ] == 'schedulings' );
    if ( $is_schedulings_entity ) {

      return Schedulings::PATCH( $resource, $logged_in, $parsed );

    }


    $is_users_entity = ( $resource[ 'entity_name' ] == 'users' );
    if ( $is_users_entity )  {

      if( isset( $parsed[ 'password' ] ) )  {

        $parsed[ 'password_hash' ] = protect_password( $parsed[ 'password' ] );
        unset( $parsed[ 'password' ] );

      }

    }

    update(
        $resource[ 'entity_name' ],
        $parsed,
        new Condition( 'id', '==', $resource[ 'entity_id' ] )
    );

    if ( $is_users_entity )  {

      $columns = array( 'id', 'name', 'username', 'unit_id' );

    }
    else {

      $columns = array( '*' );

    }

    $rows = read(
        $resource[ 'entity_name' ],
        $columns,
        $conditions= null,
        $sorting= array( 'id' => 'DESC' ),
        $splitting= [ 0, 1 ]
    );
    $new_entity = $rows[ 0 ];


    return forge_entity_table( $new_entity, $logged_in );

}

function PATCH_media_group  ( $resource, $logged_in, $parsed )  {

  trigger_error( 'parsed: ' . print_r( $parsed, true ) );

  $Selector_items = $parsed[ 'Selector_items' ];
  unset( $parsed[ 'Selector_items' ] );

  update(
      $resource[ 'entity_name' ],
      $parsed,
      new Condition( 'id', '==', $resource[ 'entity_id' ] )
  );


  /* track changes in files */
  $file_rows = read(
    'media_files_media_groups',
    array( 'media_file_id' ),
    $conditions= new Condition( 'media_group_id', '==', $resource[ 'entity_id' ] )
  );

  $received_file_ids = array();
  foreach( $Selector_items as $key => $file_id ) {
    array_push( $received_file_ids, $file_id );
  }

  $stored_file_ids = array();
  foreach( $file_rows as $key => $row ) {
    array_push( $stored_file_ids, $row[ 'media_file_id' ] );
  }
  /* ----- x ----- */
  // trigger_error( print_r( $to_create, true ) );
  // trigger_error( print_r( $to_delete, true ) );


  /* effect changes in files */
  $to_create = array_diff( $received_file_ids, $stored_file_ids );
  // trigger_error( 'to_create: ' . print_r( $to_create, true ) );
  foreach( $to_create as $key => $file_id ) {

    $data = array(
      'media_file_id' => $file_id,
      'media_group_id' => $resource[ 'entity_id' ]
    );
    create( 'media_files_media_groups', $data );

  }

  $to_delete = array_diff( $stored_file_ids, $received_file_ids );
  // trigger_error( 'to_delete: ' . print_r( $to_delete, true ) );
  foreach( $to_delete as $key => $file_id ) {

    delete(
      'media_files_media_groups',
      new Condition(
          new Condition( 'media_file_id', '==', $file_id ),
          'AND',
          new Condition( 'media_group_id', '==', $resource[ 'entity_id' ] )
      )
    );

  }
  /* ----- x ----- */


  $rows = read(
      $resource[ 'entity_name' ],
      array( '*' ),
      $conditions= new Condition( 'id', '==', $resource[ 'entity_id' ] ),
      $sorting= array( 'id' => 'DESC' ),
      $splitting= [ 0, 1 ]
  );
  $new_entity = $rows[ 0 ];

  return forge_entity_table( $new_entity, $logged_in );

}


function create_api_credential ( $user_id= null ) {

  $key = generate_api_key();

  $now = new DateTime();
  $interval = new DateInterval( 'P15M' );
  $expiration_time = $now->add( $interval );

  $data = array(
    'key' => $key ,
    'user_id' => $user_id ,
    'expiration_time' => $expiration_time->format( 'Y-m-d H:i:s' )
  );

  create( 'api_credentials', $data );

  trigger_error( 'created api_credentials' );

  return load_api_credential( $key );

}

function load_api_credential ( $api_key ) {

  $condition = new Condition( 'key', '==', $api_key );
  $credential = read(
    'api_credentials',
    array(
      '`api_credentials`.`id`',
      '`api_credentials`.`key`',
      '`api_credentials`.`expiration_time`',
      '`users`.`id` AS user_id',
      '`users`.`permission` AS user_permission'
    ),
    $conditions= $condition,
    $sorting= null,
    $splitting= null,
    $joins= array( new LeftJoin( 'api_credentials', 'user_id', 'users', 'id' ) )
  );

  if ( ! $credential )
    throw new Exception( "Não foi possível carregar chave de API" );

  return $credential[ 0 ];

}

function generate_api_key () {

  $seed = random_bytes( 512 );
  $api_key = hash( 'sha256', $seed );

  return $api_key;

}

function log_in_api_credential ( $api_key, $user_id ) {

  $data = array(
    'user_id' => $user_id
  );
  $condition = new Condition( 'key', '==', $api_key );
  update( 'api_credentials', $data, $conditions= $condition );

  $api_credentials = read( 'api_credentials', array( '*' ), $conditions= $condition );

  if ( is_null( $api_credentials[ 0 ][ 'user_id' ] ) )
    throw new Exception( "Não foi possível fazer login de chave de API" );

  return true;

}

function delete_api_credential( $api_key )  {

  $condition = new Condition( 'key', '==', $api_key );
  delete( 'api_credentials', $conditions= $condition );

  $api_credentials = read( 'api_credentials', array( '*' ), $conditions= $condition );

  return ! ( isset( $api_credentials[ 0 ]) && isset( $api_credentials[ 0 ][ 'user_id' ] ) );

}


?>
