#!/usr/bin/python3
# -*- coding: utf-8 -*-


import logging
from logging.handlers import RotatingFileHandler
import os
import shlex
from subprocess import Popen, PIPE
import time
from threading import Thread

import requests
from lxml import etree as E


class Updater ( Thread ) :


    def __init__ ( self ):

        Thread.__init__( self )
        self.daemon = True
        self.start()


    def run ( self ):

        logger.info( 'Running Updater' )

        try:
            with open( "%s/url_servidor" % script_dir, 'r' ) as fp:
                self.url_servidor = fp.read()

            with open( "%s/id_unidade_cliente" % script_dir, 'r' ) as fp:
                self.id_unidade_cliente = fp.read()

            with open( "%s/client_key" % script_dir, 'r' ) as fp:
                self.client_key = fp.read()

            with open( "%s/client_id" % script_dir, 'r' ) as fp:
                self.client_id = fp.read()


            while True:

                try:

                    sent_report = self.post_report()
                    if ( sent_report ):
                        logger.info( 'Sent report' )
                    else:
                        logger.warn( "Report couldn't be sent" )


                    lease_time = self.get_lease_time()
                    if ( lease_time ):
                        logger.info( 'Lease time updated: %d minutes' % lease_time )
                    else:
                        logger.warn( "Lease time couldn't be updated" )


                    # schedulings_ids, schedulings_xml = self.get_schedulings_ids()
                    # logger.info( 'Got schedulings_ids: %s' % schedulings_ids )
                    #
                    # media_groups_ids, media_groups_xml = self.get_media_groups_ids( schedulings_ids )
                    # logger.info( 'Got media_groups_ids: %s' % media_groups_ids )
                    #
                    # playlist_files, playlist_files_xml = self.get_playlist_files( media_groups_ids )
                    # logger.info( 'Got playlist_files: %s' % playlist_files )


                    # schedulings and playlist files inside a file
                    self.forge_schedulings_file()


                    to_create, to_delete = self.track_changes( self.playlist_files )
                    logger.info( 'to_create: %s' % to_create )
                    logger.info( 'to_delete: %s' % to_delete )

                    if ( to_create or to_delete ):
                        for file in to_create:

                            logger.info( 'Will download %s' % file )

                            with open( '%s/download/%s' % ( script_dir, file ), 'wb') as fd:

                                # download file
                                url = '%s/uploads/%s' % ( self.url_servidor, file )

                                r = requests.get( url, allow_redirects= True )

                                fd.write( r.content )

                            logger.info( 'Downloaded %s' % file )


                        with open( '%s/to_delete' % script_dir, 'w' ) as fd:

                            for file in to_delete:

                                fd.write( '%s\n' % file )

                                logger.info( 'Marked %s to delete' % file )

                        toggle = '1'

                    else:

                        toggle = '0'


                    # toggle
                    with open( '%s/files_changed' % script_dir, 'w' ) as fd:

                        fd.write( toggle )

                except Exception as e:

                    logger.exception( e )

                logger.info( 'Ended update. Gonna lease %d minutes' % lease_time )

                time.sleep( lease_time * 60 )


        except Exception as e:

            logger.exception( e )


    def post_report ( self ):

        url = "%s/players_reports?client_key=%s" % (
            self.url_servidor,
            self.client_key
        )

        r = requests.post(
            url,
            data= {
                'ip' : self.get_ip(),
                'player_id' : self.client_id
            }
        )

        result = r.status_code == 200

        return result


    def get_ip ( self ):

        run = Popen( shlex.split( 'hostname -I' ), stdout= PIPE)
        stdout, stderr = run.communicate()

        ip = stdout[ :-2 ]

        return ip


    def get_lease_time ( self ):

        url = "%s/players_config/search?client_key=%s&id=1" % (
            self.url_servidor,
            self.client_key
        )

        r = requests.get( url )

        if ( r.status_code != 200 ):
            return False

        tree = E.fromstring( r.text.encode( 'utf-8' ) )

        lease_time = tree.xpath( "//lease_time" )[ 0 ].text

        return int( lease_time )


    # def get_schedulings_ids ( self ):
    #
    #     url = "%s/schedulings/search?client_key=%s&id=%s" % (
    #         self.url_servidor,
    #         self.client_key,
    #         self.id_unidade_cliente
    #     )
    #
    #     r = requests.get( url )
    #
    #     if ( r.status_code != 200 ):
    #         raise Exception()
    #
    #     logger.debug( 'r.text: %s' % r.text )
    #
    #     tree = E.fromstring( r.text.encode( 'utf-8' ) )
    #
    #     schedulings_ids = []
    #
    #     ids = tree.xpath( "//row/id" )
    #     for id in ids:
    #         schedulings_ids.append( id.text )
    #
    #     return ( schedulings_ids, tree )


    def get_schedulings_xml ( self ):

        url = "%s/schedulings/search?client_key=%s&unit_id=%s" % (
            self.url_servidor,
            self.client_key,
            self.id_unidade_cliente
        )

        logger.debug( 'url: %s' % url )

        r = requests.get( url )

        if ( r.status_code != 200 ):
            raise Exception()

        logger.debug( 'r.text: %s' % r.text )

        tree = E.fromstring( r.text.encode( 'utf-8' ) )

        return tree


    # def get_media_groups_ids ( self, schedulings_ids ):
    #
    #     url = "%s/media_groups_schedulings/search?client_key=%s" % (
    #         self.url_servidor,
    #         self.client_key
    #     )
    #
    #     media_groups_ids = []
    #
    #     for scheduling_id in schedulings_ids:
    #
    #         each_url = '%s&scheduling_id=%s' % ( url, scheduling_id )
    #
    #         r = requests.get( each_url )
    #         logger.debug( 'r.status_code: %d' % r.status_code )
    #         logger.debug( 'r.text: %s' % r.text )
    #
    #         xmldoc = E.fromstring( r.text.encode( 'utf-8' ) )
    #
    #         ids = xmldoc.xpath( '//row/id' )
    #         for id in ids:
    #
    #             media_groups_ids.append( id.text )
    #
    #     return ( media_groups_ids, xmldoc )


    def get_media_groups_ids ( self, column= 'scheduling_id', value= None ):

        url = "%s/media_groups_schedulings/search?client_key=%s" % (
            self.url_servidor,
            self.client_key
        )

        media_groups_ids = []

        each_url = '%s&%s=%s' % ( url, column, value )

        r = requests.get( each_url )
        logger.debug( 'r.status_code: %d' % r.status_code )
        logger.debug( 'r.text: %s' % r.text )

        xmldoc = E.fromstring( r.text.encode( 'utf-8' ) )

        ids = xmldoc.xpath( '//row/id' )
        for id in ids:

            media_groups_ids.append( id.text )

        return media_groups_ids


    def get_playlist_files ( self, media_groups_ids ):

        url = "%s/media_files_media_groups/search?client_key=%s" % (
            self.url_servidor,
            self.client_key
        )

        files = set()

        for media_group_id in media_groups_ids:

            each_url = "%s&media_group_id=%s" % ( url, media_group_id )

            r = requests.get( each_url )

            xmldoc = E.fromstring( r.text.encode( 'utf-8' ) )

            paths = xmldoc.xpath( '//row/path' )
            for path in paths:

                files.add( path.text )

        return files


    def track_changes ( self, playlist_files ):

        playlist_dir = '%s/playlist' % script_dir

        current_playlist_files = set( [ f for f in os.listdir( playlist_dir ) if os.path.isfile( '%s/%s' % ( playlist_dir, f ) ) ] )

        to_create = playlist_files - current_playlist_files
        to_delete = current_playlist_files - playlist_files

        return ( to_create, to_delete )


    def forge_schedulings_file ( self ) :

        # logger.debug( 'HERE' )

        schedulings = E.Element( 'schedulings' )

        # logger.debug( 'HERE 1' )

        schedulings_xml = self.get_schedulings_xml()

        # logger.debug( 'HERE 2.5' )

        # all_playlist_files = set()
        # self.playlist_files = set()
        self.flush_playlist()

        # logger.debug( 'HERE 3' )

        has_scheduling_now = False

        # logger.debug( 'schedulings_xml: %s' % schedulings_xml )

        rows = schedulings_xml.xpath( "//row" )
        for row in rows:

            scheduling = E.SubElement( schedulings, 'scheduling' )

            start = row.find( 'start' )
            scheduling.append( start )

            start_date = start.text

            end = row.find( 'end' )
            scheduling.append( end )

            end_date = end.text


            import datetime
            now = datetime.datetime.now()
            start_date_obj = datetime.datetime.strptime( start_date, '%Y-%m-%d %H:%M:%S' )
            end_date_obj = datetime.datetime.strptime( end_date, '%Y-%m-%d %H:%M:%S' )

            logger.debug( 'now: %s' % now )
            logger.debug( 'start_date: %s' % start_date )
            logger.debug( 'end_date: %s' % end_date )

            if start_date_obj <= now and end_date_obj > now:

                has_scheduling_now = True
                logger.debug( 'Has scheduling now!' )


            scheduling_id = row.find( 'id' ).text
            logger.debug( 'scheduling_id: %s' % scheduling_id )

            if scheduling_id is None:

                media_groups_ids = [ 1 ]

                # logger.debug( 'NOOONE' )
                #
                # unit_id = row.find( 'unit_id' ).text
                # media_groups_ids = self.get_media_groups_ids( column= 'unit_id', value= unit_id )
                # logger.debug( 'media_groups_ids: %s' % media_groups_ids )

            else:

                media_groups_ids = self.get_media_groups_ids( value= scheduling_id )

            playlist_files = self.get_playlist_files( media_groups_ids )

            self.add_files_to_playlist( playlist_files, scheduling )

            logger.debug( playlist_files )


            # for playlist_file in playlist_files:
            #
            #     # logger.debug( playlist_file )
            #
            #     file = E.SubElement( scheduling, 'file' )
            #     file.text = playlist_file
            #
            #     all_playlist_files.add( playlist_file )


        # if not has_scheduling_now :
        #
        #     logger.debug( 'No scheduling now: %s' % now )
        #
        #     scheduling = E.SubElement( schedulings, 'scheduling' )
        #
        #     playlist_files = self.get_playlist_files( 1 ) # ID de grupo de mídia "Padrão"
        #
        #     self.add_files_to_playlist( playlist_files, scheduling )


        from pprint import pprint
        logger.debug( 'schedulings: %s' % pprint( E.tostring( schedulings ) ) )


        with open( '%s/schedulings.xml' % script_dir, 'w' ) as fp:

            fp.write( E.tostring( schedulings, encoding='utf-8', pretty_print=True, xml_declaration= True ).decode( 'utf-8' ) )


    def add_files_to_playlist ( self, playlist_files, scheduling ) :

        for playlist_file in playlist_files:

            file = E.SubElement( scheduling, 'file' )
            file.text = playlist_file

            self.playlist_files.add( playlist_file )


    def flush_playlist ( self ) :

        self.playlist_files = set()



script_dir = os.path.dirname( os.path.realpath( __file__ ) )


# Configure logger
logger = logging.getLogger( __name__ )
logger.setLevel( logging.DEBUG )

logfile = '%s/log/updater.log' % script_dir

fh = RotatingFileHandler( logfile, mode='a', maxBytes=1024*1024, backupCount= 0, encoding= "utf-8", delay= False )
fh.setLevel( logging.DEBUG )

formatter = logging.Formatter( '%(asctime)s - %(levelname)s - %(funcName)s - %(message)s' )
fh.setFormatter( formatter )

logger.addHandler( fh )
