#!/bin/bash


# Verifica usuário que executa o script é root
if [ `id -u` -ne 0 ]; then
  echo "Execute este script como root."
  exit
fi


# Configura /etc/nsswitch.conf
sed -i "s/hosts:/#hosts:/g" /etc/nsswitch.conf
echo "" >> /etc/nsswitch.conf
echo "hosts: files dns myhostname" >> /etc/nsswitch.conf


# Pede variável do proxy para usuário
echo "Digite a variável do proxy de rede."
echo "Formato: http://usuário:senha@servidor:porta"
echo ""
echo -n "Proxy: "
read proxy
echo ""


# Configura proxy no /etc/environment
echo "" >> /etc/environment
echo "http_proxy=\"$proxy\"" >> /etc/environment
echo "https_proxy=\"$proxy\"" >> /etc/environment
echo "ftp_proxy=\"$proxy\"" >> /etc/environment


# Configura variáveis de ambiente responsáveis por proxy
export http_proxy="$proxy"
export https_proxy="$proxy"
export ftp_proxy="$proxy"


# Configura proxy no APT
cat > /etc/apt/apt.conf.d/80proxy << EOF
Acquire::http::proxy "$proxy";
Acquire::https::proxy "$proxy";
Acquire::ftp::proxy "$proxy";
EOF


# Atualiza índice de pacotes
apt-get update

# Atualiza pacotes desatualizados
apt full-upgrade -y --allow-downgrades --allow-remove-essential --allow-change-held-packages

# Instala pacotes necessários
apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages python3-pip libxslt-dev lshw vlc

# Instala módulos Python necessários
pip3 install lxml requests python-crontab


# Cria diretório /home/mediashare/.ssh
mkdir /home/mediashare/.ssh
chmod -R 777 /home/mediashare/.ssh
chown -R mediashare:mediashare /home/mediashare/.ssh


# Efetua configuração para netbook Philco

# Instala pacotes extra
apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages sudo fbi v86d psmisc alsa-utils


# Desativa driver de vídeo do Philco
echo "blacklist gma500_gfx" > /etc/modprobe.d/gfx-blacklist.conf

# Ativa FrameBuffer
modprobe uvesafb
echo "" >> /etc/modules
echo uvesafb >> /etc/modules


# Configura sudo
echo '' | EDITOR='tee -a' visudo
echo 'mediashare      ALL=(ALL) NOPASSWD: /usr/bin/fbi, /usr/bin/killall, /sbin/reboot' | EDITOR='tee -a' visudo


# Atualiza configuração GRUB
sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT=\"quiet\"/GRUB_CMDLINE_LINUX_DEFAULT=\"vt.global_cursor_default=0 consoleblank=0\"/g" /etc/default/grub
update-grub


reboot
