#!/bin/bash


echo "Script de Configuração para Cliente MediaShare"
echo "versão 1.0 - GPLv3 - por Edgar Roberto Lemke <edgar_lemke@yahoo.com.br>"
echo ""


# Verifica usuário que executa o script é root
if [ `id -u` -ne 0 ]; then
  echo "Execute este script como root."
  exit
fi


# Atualiza hostname
echo -n "Digite o hostname do equipamento: "
read new_hostname

old_hostname=`cat /etc/hostname`

echo "$new_hostname" > /etc/hostname
sed -i "s/$old_hostname/$new_hostname/g" /etc/hosts


cd /home/mediashare/mediashare

# Obtém endereço do servidor
echo -n "Digite o domínio ou endereço IP do servidor MediaShare: "
read server_addr
echo "$server_addr" > server_addr

# Baixa cliente
url_cliente="http://$server_addr/client/zotac/client/"
wget -q --cut-dirs=4 -nH -l1 -r --no-parent --reject="index.html*" "$url_cliente"
echo ""

chmod +x *.py
chmod +x *.bash


# Roda setup.py
python3 setup.py


# Configura start.py na inicialização
mkdir -p /home/mediashare/.config/openbox
cat > /home/mediashare/.config/openbox/autostart.sh << EOF
#!/bin/bash

/usr/bin/python3 /home/mediashare/mediashare/start.py
EOF
chown -R mediashare:mediashare /home/mediashare/.config/openbox/
chmod +x /home/mediashare/.config/openbox/autostart.sh


mkdir /home/mediashare/mediashare/playlist
mkdir /home/mediashare/mediashare/download
mkdir /home/mediashare/mediashare/log
touch /home/mediashare/mediashare/log/{start.log,player.log,updater.log}
chmod -R 777 /home/mediashare/mediashare


# Reinicia
echo ""
echo "Reiniciando em 5 segundos"

c=5
while [ $c -gt 0 ]; do
  echo -n ". "
  c=$((c-1))
  sleep 1
done
echo ""

reboot
