#!/usr/bin/python3
# coding: utf-8


from datetime import datetime
import logging
from logging.handlers import RotatingFileHandler
import os
from pprint import pprint
import random
import shlex
from subprocess import Popen, PIPE
from time import sleep
from threading import Thread

from lxml import etree as E


class Player ( Thread ) :

    def __init__ ( self ):

        Thread.__init__( self )
        self.daemon = True
        self.start()

    def run ( self ):

        try:

            logger.info( 'Running Player' )

            # Blank screen
            #p = Popen( shlex.split( '/opt/vc/bin/tvservice -o' ), stdout= PIPE, stderr= PIPE )
            #p.wait()

            #p = Popen( shlex.split( '/opt/vc/bin/tvservice -p' ), stdout= PIPE, stderr= PIPE )
            #p.wait()



            while True:

                with open( '%s/files_changed' % script_dir, 'r+' ) as fd:

                    files_changed = fd.read() == '1'

                    logger.debug( 'Files changed: %s' % files_changed )

                    if files_changed:
                        fd.write( '0' )


                if files_changed:

                    with open( '%s/to_delete' % script_dir , 'r' ) as fd:

                        to_delete = fd.readlines()

                    for file in to_delete:

                        file = file[:-1]

                        try:
                            os.remove( '%s/playlist/%s' % ( script_dir, file ) )
                            logger.info( 'Deleted %s' % file )

                        except FileNotFoundError:
                            logger.exception( "Couldn't delete %s" % file )


                    downloaded_files = os.listdir( '%s/download' % script_dir )
                    for file in downloaded_files:

                        try:
                            os.rename (
                                '%s/download/%s' % ( script_dir, file ),
                                '%s/playlist/%s' % ( script_dir, file )
                            )
                            logger.info( 'Moved %s' % file )

                        except FileNotFoundError:
                            logger.exception( "Couldn't move %s" % file )



                with open( '%s/schedulings.xml' % script_dir, 'r' ) as fd:

                    data = fd.read()

                    logger.debug( 'Data from schedulings.xml:\n%s' % data )

                    try:
                        tree = E.fromstring( data.encode( 'utf-8' ) )
                    except:
                        continue

                    schedulings = tree.xpath( "//scheduling" )


                found_scheduling = False
                for scheduling in schedulings:

                    start = scheduling.find( 'start' )
                    start_date = datetime.strptime( start.text, '%Y-%m-%d %H:%M:%S' )

                    end = scheduling.find( 'end' )
                    end_date = datetime.strptime( end.text, '%Y-%m-%d %H:%M:%S' )

                    now = datetime.now()


                    if start_date > now or end_date < now:
                        logger.info( 'Skipping scheduling - start: %s - end: %s - now: %s' % ( start_date, end_date, now ) )
                        continue


                    logger.info( 'Scheduling match - start: %s - end: %s - now: %s' % ( start_date, end_date, now ) )

                    files = scheduling.findall( 'file' )

                    playlist_files = []
                    for file in files:
                        playlist_files.append( file.text )

                    random.shuffle( playlist_files )

                    logger.info( 'Playlist: %s' % playlist_files )

                    for file in playlist_files:

                        # Restore ALSA configuration
                        #cmd = '/usr/sbin/alsactl -F -f %s/alsa.conf restore' % script_dir
                        #alsa_proc = Popen( shlex.split( cmd ) )
                        #alsa_proc.wait()


                        path = '%s/playlist/%s' % ( script_dir, file )

                        #cmd = 'sudo -u mediashare cvlc -v -f --no-video-title --video-on-top --no-fb-tty --play-and-exit --vout fb,none -A alsa,none --alsa-audio-device plughw:0,3 "%s"' % path
                        cmd = 'cvlc -v -f --no-video-title --video-on-top --play-and-exit "%s"' % path
                        logger.info( 'Player command line: %s' % cmd )

                        p = Popen( shlex.split( cmd ), stdout= PIPE, stderr= PIPE )

                        logger.info( 'Playing %s' % file )

                        stdout, stderr = p.communicate()

                        logger.info( 'Ended playing %s' % file )

                        logger.debug( 'Player stdout: %s' % stdout )
                        logger.debug( 'Player stderr: %s' % stderr )

                    # Break out of for loop back into While True loop
                    found_scheduling = True
                    break


                if not found_scheduling:

                    self.show_wallpaper()



        except Exception as e:

            logger.exception( '' )


    def show_wallpaper ( self ) :

        p = Popen( shlex.split( 'sudo fbi -T 1 -noverbose -a "%s/wallpaper.png"' % script_dir ) )
        p.wait()

        sleep( 60 )

        logger.info( 'Killling fbi' )
        p2 = Popen( shlex.split( 'sudo killall fbi' ), stdout= PIPE, stderr= PIPE )

        stdout, stderr = p2.communicate()

        # logger.info( 'killall stdout: %s' % stdout )
        # logger.info( 'killall stderr: %s' % stderr )




script_dir = os.path.dirname( os.path.realpath( __file__ ) ) # 8513


# Configure logger
logger = logging.getLogger( __name__ )
logger.setLevel( logging.DEBUG )

logfile = '%s/log/player.log' % script_dir

fh = RotatingFileHandler( logfile, mode='a', maxBytes=1024*1024, backupCount= 0, encoding= "utf-8", delay= False )
fh.setLevel( logging.DEBUG )

formatter = logging.Formatter( '%(asctime)s - %(levelname)s - %(funcName)s - %(message)s' )
fh.setFormatter( formatter )

logger.addHandler( fh )
