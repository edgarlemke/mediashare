#!/usr/bin/python3
# -*- coding: utf-8 -*-


import getpass
import os
import requests
import shlex
from subprocess import Popen, PIPE

from crontab import CronTab
from lxml import etree as e


CLIENT_VERSION = "1.0"


class Setup:


    def __init__ ( self ):

        try:

            self.url_servidor = self.obtem_url_servidor()

            self.api_key = self.obtem_api_key()



            unidades = self.obtem_unidades()

            self.id_unidade_cliente = self.obtem_id_unidade_cliente( unidades )
            print( 'ID Unidade Cliente: %s' % self.id_unidade_cliente )

            self.client_id, self.client_key = self.obtem_client_key()
            print( 'ID do Cliente: %s - Chave do Cliente: %s' % ( self.client_id, self.client_key ) )


            with open( "%s/client_key" % script_dir, 'w' ) as fp:

                fp.write( self.client_key )

            with open( "%s/client_id" % script_dir, 'w' ) as fp:

                fp.write( str( self.client_id ) )

            with open( "%s/url_servidor" % script_dir, 'w' ) as fp:

                fp.write( self.url_servidor )

            with open( "%s/id_unidade_cliente" % script_dir, 'w' ) as fp:

                fp.write( self.id_unidade_cliente )


            self.download_ssh_key()

#            self.setup_crontab()


        except Exception as e:

            print( e )


    def obtem_url_servidor ( self ) :

        with open( '%s/server_addr' % script_dir, 'r' ) as fp:

            lines = fp.readlines()

        server_addr = lines[0].split( '\n' )[0]
        url_servidor = 'http://%s' % server_addr

        return url_servidor


    def obtem_api_key ( self ) :

        url_login = "%s/login" % self.url_servidor

        usuario = input( "Digite o login: " )
        senha = getpass.getpass( prompt="Digite a senha: " )

        r = requests.post(
            url_login,
            data= {
                'username':usuario,
                'password':senha
            }
        )

        #print(r.text)

        try:
            api_key = r.url.split( '=' )[ 1 ]

        except:
            raise Exception( "Não foi possível efetuar login no servidor." )

        return api_key


    def obtem_unidades ( self ) :

        unidades = []
        url_busca_unidades = "%s/units/search?api_key=%s" % (
            self.url_servidor,
            self.api_key
        )

        r = requests.get(
            url_busca_unidades
        )

        xml_unidades = e.fromstring( r.text.encode( 'utf-8' ) )

        unidades = []
        rows = xml_unidades.xpath( '//row' )

        for row in rows:

             children = row.getchildren()
             t = tuple( [ children[0].text, children[1].text ] )
             unidades.append( t )

        return unidades


    def obtem_id_unidade_cliente ( self, unidades ):

        print( "\n-----------------------")
        print( "Lista de Unidades" )
        print( "-----------------------")
        print( "ID\t|\tUnidade")
        print( "-----------------------")

        for id, unidade in unidades:

            print( "%s\t|\t%s" % ( id, unidade ) )

        print( "-----------------------\n" )

        id_unidade_cliente = input(
            "Digite ID da unidade em que este cliente será instalado: "
        )

        if not id_unidade_cliente.isnumeric():
            raise Exception( "ID da unidade fornecido é inválido." )


        return id_unidade_cliente


    def obtem_client_key ( self ):

        url_players = '%s/players?api_key=%s' % (
            self.url_servidor,
            self.api_key
        )

        client_data = self.get_client_data()

        print( "" )
        print( 'Dados do cliente: %s' % client_data )

        try:
            r = requests.post(
                url_players,
                data= client_data
            )

            if r.status_code != requests.codes.ok:
                raise Exception( "Não foi possível registrar cliente." )

            client_key = r.text

            print( "" )
            print( "Cliente registrado com sucesso." )
            #print( "Chave: %s" % client_key )

            url_search_players = '%s/players/search?client_key=%s' % (
                self.url_servidor,
                client_key
            )

            r2 = requests.get(
                url_search_players
            )

            if r.status_code != requests.codes.ok:
                raise Exception( "Não foi possível retornar ID de cliente." )


            tree = e.fromstring( r2.text.encode( 'utf-8' ) )
            id = int( tree.xpath( '//id' )[0].text )

        except Exception as exc:

            print( 'exc: %s' % exc )

        return ( id, client_key )


    def get_client_data ( self ) :

        p = Popen(
            shlex.split( "lshw -class system -xml" ),
            stdout= PIPE,
            stderr= PIPE
        )

        stdout, stderr = p.communicate()

        # print( 'lxml stdout: %s' % stdout )

        xmldoc = e.fromstring( stdout )


        # hostname = xmldoc.xpath( "//node[@class='system']/@id" )[0]
        p2 = Popen(
            shlex.split( "hostname" ),
            stdout= PIPE
        )
        
        stdout, stderr = p2.communicate()
        hostname = str(stdout)
        print(hostname)
        
        
        try:
            vendor = xmldoc.xpath( "//vendor" )[0].text
        except:
            vendor = ''

        try:
            hardware = xmldoc.xpath( "//product" )[0].text
        except:
            hardware = ''

        try:
            serial = xmldoc.xpath( "//serial" )[0].text
        except:
            serial = ''


        client_data= {
            'hostname' : hostname,
            'vendor' : vendor,
            'hardware' : hardware,
            'serial' : serial,
            'client_version' : CLIENT_VERSION,
            'unit_id' : self.id_unidade_cliente

        }

        return client_data


    def download_ssh_key ( self ):

        url = "%s/ssh/mediashare.pub" % self.url_servidor

        r = requests.get( url )

        with open( '/home/mediashare/.ssh/authorized_keys', 'wb' ) as fd:

            fd.write( r.content )


#    def setup_crontab ( self ) :
#
#        cron = CronTab( user= 'root' )
#        job = cron.new( command='/usr/bin/python3 /home/mediashare/mediashare/start.py' )
#        job.every_reboot()
#        cron.write()


script_dir = os.path.dirname( os.path.realpath( __file__ ) )


Setup()
