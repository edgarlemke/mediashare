#!/usr/bin/python3
# coding: utf-8


import logging
from logging.handlers import RotatingFileHandler
import os
import time

from updater import Updater
from player import Player


# Configure logger
script_dir = os.path.dirname( os.path.realpath( __file__ ) )

logger = logging.getLogger( __name__ )
logger.setLevel( logging.DEBUG )

logfile = '%s/log/start.log' % script_dir

fh = RotatingFileHandler( logfile, mode='a', maxBytes=1024*1024, backupCount= 0, encoding= "utf-8", delay= False )
fh.setLevel( logging.DEBUG )

formatter = logging.Formatter( '%(asctime)s - %(levelname)s - %(funcName)s - %(message)s' )
fh.setFormatter( formatter )

logger.addHandler( fh )


time.sleep( 30 )


logger.info( 'Starting MediaShare' )


Updater()
Player()


while True:
    time.sleep( 1 )
