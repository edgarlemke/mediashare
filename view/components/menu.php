<?php

// include_once( 'controller.php' );
include_once( 'config.php' );
include_once( 'controller/auth.php' );

class Menu {

  private $labels = array(
    'players_config' => 'Players',
    'media_files' => 'Mídia',
    'media_groups' => 'Grupos de Mídia',
    'users' => 'Usuários' ,
    'units' => 'Unidades',
    'schedulings' => 'Agendamentos'
  );

  public function __construct ( $node ) {

    global $credential; # controller.php
    global $permission_level; # auth.php


    $dom = new DOMDocument( '1.0', 'utf-8' );

    $clone = $node->cloneNode( true );

    $table = $dom->importNode( $clone );
    $table->setAttribute( 'class', 'Menu table' );
    $dom->appendChild( $table );

    $tbody = $dom->createElement( 'div' );
    $tbody->setAttribute( 'class', 'table_body' );
    $table->appendChild( $tbody );

    $row = $dom->createElement( 'div' );
    $row->setAttribute( 'class', 'table_row' );
    $tbody->appendChild( $row );


    $cell = $dom->createElement( 'div' );
    $cell->setAttribute( 'class', 'table_cell menu_button' );

    foreach( $permission_level as $collection => $level ) {

      if( $credential[ 'user_permission' ] < $level )
        continue;

      $cell_clone = $cell->cloneNode( true );
      $row->appendChild( $cell_clone );

      $a = $dom->createElement( 'a' );

      if( $collection == 'players_config' ) {
        $href = $public_root . $collection . '/1/edit?api_key=$api_key';
      }
      else {
        $href = $public_root . $collection . '?api_key=$api_key';
      }
      $a->setAttribute( 'href', $href );

      $textNode = $dom->createTextNode( $this->labels[ $collection ] );
      $a->appendChild( $textNode );

      $cell_clone->appendChild( $a );

    }


    $this->template = $dom->saveHTML();

  }

  public function output () {

    return $this->template;

  }

}

?>
