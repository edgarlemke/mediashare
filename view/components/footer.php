<?php


class Footer {

  public function __construct ( $node )  {

    $this->template = '<footer>
  Responsável: Edgar Roberto Lemke<br>
  Assistente de TI - Setor de Informática - Secretaria da Saúde<br>
  2106-8468  -  id10766@jaraguadosul.sc.gov.br<br>
  Desenvolvido e mantido por Edgar Roberto Lemke sob licença GPLv3.
</footer>';

  }

  public function output () {

    return $this->template;

  }

}


?>
