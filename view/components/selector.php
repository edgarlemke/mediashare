<?php

class Selector  {

  public function __construct ( $node ) {

    $data_table = $node->getAttribute( 'data-table' );
    $data_column = $node->getAttribute( 'data-column' );
    $data_column_label = $node->getAttribute( 'data-column-label' );
    $data_relationship_table = $node->getAttribute( 'data-relationship-table' );
    $data_relationship_column = $node->getAttribute( 'data-relationship-column' );

    $template = '<div class="Selector" data-table="'.$data_table.'" data-column="'.$data_column.'" data-relationship-table="'.$data_relationship_table.'" data-relationship-column="'.$data_relationship_column.'">
  <table class="Selector_items_table">
    <thead>
      <tr>
        <th>'.$data_column_label.'</th>
        <th>Excluir</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <br>
  <div>
    <input type="text" class="Selector_search" onfocusin="Selector.enable_field( this );" placeholder="Busca" readonly>
  </div>
</div>';

    $this->template = $template;

  }

  public function output  ()  {

    $output = $this->template;

    return $output;

  }

}

?>
