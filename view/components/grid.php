<?php


class Grid {


  public static $labels = array(

    'id' => 'ID',
    'inclusion' => 'Inclusão',
    'last_connection' => 'Última conexão',
    'so_string' => 'Sistema Operacional',
    'user_id' => 'Usuário',
    'timestamp' => 'Data/Hora',
    'path' => 'Arquivo',
    'username' => 'Login',
    'user_username' => 'Login',
    'name' => 'Nome',
    'units_id' => 'Unidade',
    'units_name' => 'Unidade',
    'permission' => 'Permissão',
    'start' => 'Início',
    'end' => 'Fim',
#    'media_group_id' => 'Grupo de Mídia',
    'media_groups_name' => 'Grupo de Mídia',
    'ip' => 'IP',
    'last_report' => 'Último relatório',
    'hardware' => 'Hardware',
    'serial' => 'Serial',
    'client_version' => 'Versão do Cliente',
    'hostname' => 'Hostname'

  );


  public function __construct ( $node ) {


    /* Por padrão o atributo data-table define a tabela que o Grid gerencia */
    if ( ! $node->hasAttribute( 'data-table' ) )  {
        throw new Exception( "Grid node has no data-table attribute: " . print_r( $node, true ) );
    }
    $this->target_table = $node->getAttribute( 'data-table' );

    /* Por padrão o atributo data-search-uri define a URI que o Grid faz buscas */
    if ( ! $node->hasAttribute( 'data-search-uri' ) )  {
        throw new Exception( "Grid node has no data-search-uri attribute: " . print_r( $node, true ) );
    }
    $this->search_uri = $node->getAttribute( 'data-search-uri' );


    $thead = Grid::get_thead( $this->target_table );

    $template = "<div class=\"Grid\" data-table=\"{$this->target_table}\" data-search-uri=\"{$this->search_uri}\">
    <div style=\"position: relative; height: 95%; overflow-y: auto;\">
    <table>
      $thead
      <tbody></tbody>
    </table>
  </div>
</div>";

    $this->output = $template;

  }

  private static function get_thead ( $table )  {

    $ths='';

    if (    $table == 'users'    )    {

        $columns= array( 'id', 'name', 'username', 'units_name', 'permission' );

    }
    else if ( $table == 'media_files' ) {

      $columns = array( 'id', 'path', 'user_username', 'timestamp' );

    }
    else if ( $table == 'schedulings' ) {

      $columns = array( 'id', 'start', 'end', 'units_name' );

    }
    else if ( $table == 'players' ) {

      $columns = array( 'id', 'units_name', 'ip', 'last_report', 'inclusion', 'hostname', 'hardware', 'serial', 'client_version' );

    }
    else    {

      $columns = get_columns( $table );

    }

    foreach( $columns as $col ) {

      $label = ( isset( Grid::$labels[ $col ] ) ? Grid::$labels[ $col ] : $col );

      if ( $table == 'users' && $col == 'permission' )  {
        $input = '<select name="permission">
        <option>'. $label .'</option>
        <option value="0">Inativo</option>
        <option value="1">Usuário local</option>
        <option value="2">Gerente</option>
        <option value="3">Administrador</option>
        </select>';
      }
      else {
        $input = '<input type="text" name="'. $col . '" placeholder="' . $label . '">';
      }

      $ths .= "<th>$input</th>";

    }

    $html = "<thead><tr>$ths<th>Ações</th></tr></thead>";

    // print( '<!--' . $html . '-->' );

    return $html;

  }

  public function output() {

    return $this->output;

  }

}


?>
