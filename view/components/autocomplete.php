<?php


class AutoComplete  {

  public function __construct ( $node ) {

    $needed_attributes = array( 'name', 'data-table', 'data-column', 'data-hidden-name' );

    foreach( $needed_attributes as $attribute ) {

      if ( ! $node->hasAttribute( $attribute ) )  {

        throw new Exception( "AutoComplete node has no $attribute attribute: " . print_r( $node, true ) );

      }

    }

    $node->setAttribute( 'readonly', 'true' );
    $node->setAttribute( 'onfocusin', 'new AutoComplete( this );' );
    // $node->setAttribute( 'onfocusout', 'AutoComplete.remove_table( this.getAttribute( \'data-table\' ) ); this.readOnly = true;' );
    // $node->setAttribute( 'onfocusout', 'alert( "onfocusout" ); this.readOnly = true;' );
    // $node->setAttribute( 'onkeyup', 'AutoComplete.search( this );' );

    $this->node = $node;

    $data_hidden_name = $node->getAttribute( 'data-hidden-name' );

    $this->hidden_input = '<input type="hidden" name="'.$data_hidden_name.'">';

  }

  public function output() {

    $output = $this->node->ownerDocument->saveHTML( $this->node ) . $this->hidden_input;
    trigger_error( $output );

    return $output;

  }

}


?>
