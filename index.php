<?php

/*

Arquivo INDEX.PHP

Serve como porta de entrada para as requisições dos recursos (REST).

*/

include_once( 'controller.php' );

handle_request();

?>
